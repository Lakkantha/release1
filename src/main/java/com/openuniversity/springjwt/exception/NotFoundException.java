package com.openuniversity.springjwt.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
