package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.dto.ForeignApplicantDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.service.custom.ForeignApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/foreignApplicants")
@CrossOrigin
@RestController
public class ForeignApplicantController {

    @Autowired
    private ForeignApplicantService foreignApplicantService;


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ForeignApplicantDTO getForeignApplicant(@PathVariable int id) {
        try {
            return foreignApplicantService.findForeignApplicant(id);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveForeignApplicant(@RequestBody ForeignApplicantDTO applicantDTO) {

        foreignApplicantService.saveApplicant(applicantDTO);
        return "\"" + applicantDTO  .getId() + "\"";
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteForeignApplicant(@PathVariable int id) {
        foreignApplicantService.deleteApplicant(id);
    }
/*
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void updateForeignApplicant(@PathVariable int id,
                               @RequestBody ForeignApplicantDTO applicant) {
        applicant.setId(id);
        foreignApplicantService.updateApplicant(applicant);
    }*/
}
