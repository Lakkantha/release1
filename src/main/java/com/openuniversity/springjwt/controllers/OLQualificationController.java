package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;
import com.openuniversity.springjwt.models.ALQualifications;
import com.openuniversity.springjwt.models.OLCertificates;
import com.openuniversity.springjwt.models.OLQualification;
import com.openuniversity.springjwt.models.OLSubjects;
import com.openuniversity.springjwt.repository.OLCertificatesRepository;
import com.openuniversity.springjwt.repository.OLQualificationRepository;
import com.openuniversity.springjwt.repository.OLSubjectsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class OLQualificationController {

    @Autowired
    private OLQualificationRepository olRepo;

    @Autowired
    private OLSubjectsRepository OlSubjectRepo;

    @Autowired
    private OLCertificatesRepository olCertiRepo;

    // list all OL subjects
    @GetMapping("/ol_subjects")
    public List<OLSubjects> getAllOLSubjects(){
        return OlSubjectRepo.findAll();
    }

    // list all OL data
    @GetMapping("/ol_data")
    public List<OLQualification> getAllOLData(){
        return olRepo.findAll();
    }

    // save OL data
    @PostMapping("/ol_data")
    public OLQualification createOLData(@RequestBody OLQualification olQualification) {
        return olRepo.save(olQualification);
    }

    // update OL data
    @PutMapping("/ol_data/{id}")
    public ResponseEntity<OLQualification> updateAL(@PathVariable(value = "id") Long id,
                                                     @Valid @RequestBody OLQualification oldata) throws ResourceNotFoundException {
        OLQualification ol = olRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

        ol.setYear(oldata.getYear());
        ol.setIndexNumber(oldata.getIndexNumber());
        ol.setMedium(oldata.getMedium());
        ol.setOlSubjects(oldata.getOlSubjects());
        ol.setResult(oldata.getResult());

        final OLQualification updatedOlData = olRepo.save(ol);
        return ResponseEntity.ok(updatedOlData);
    }

    // delete OL data
    @DeleteMapping("/ol_data/{id}")
    void deleteOLData(@PathVariable Long id) {
        olRepo.deleteById(id);
    }

    // list all OL Certificates data
    @GetMapping("/ol_certificates")
    public List<OLCertificates> getAllOLCertificatesData(){
        return olCertiRepo.findAll();
    }

    // save OL Certificates data
    @PostMapping("/ol_certificates")
    public OLCertificates createOLCertificates(@RequestBody OLCertificates oLCertificates) {
        return olCertiRepo.save(oLCertificates);
    }

    // delete OL Certificates data
    @DeleteMapping("/ol_certificates/{id}")
    void deleteOLCertificates(@PathVariable Long id) {
        olCertiRepo.deleteById(id);
    }

}
