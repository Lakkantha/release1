package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.dto.CourseDTO;
import com.openuniversity.springjwt.dto.CourseSelectionDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.service.custom.CourseService;
import com.openuniversity.springjwt.service.custom.ProgramApplicableFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/v1/courseSelection")
@CrossOrigin
@RestController
public class ProgramApplicableFeeController {
    @Autowired
    private ProgramApplicableFeeService programApplicableFeeService;

    @GetMapping(value = "/{mobile}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CourseSelectionDTO getDetailsForCourseSelection(@PathVariable String mobile) {

        try {
            CourseSelectionDTO detailsForCourseSelection = programApplicableFeeService.getDetailsForCourseSelection(mobile);
            return detailsForCourseSelection;
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }
}
