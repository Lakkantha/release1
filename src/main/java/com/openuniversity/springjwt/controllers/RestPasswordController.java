package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.models.ConfirmationToken;
import com.openuniversity.springjwt.models.User_Applicant;
import com.openuniversity.springjwt.payload.request.ResetPasswordRequest;
import com.openuniversity.springjwt.payload.response.MessageResponse;
import com.openuniversity.springjwt.repository.ConfirmationTokenRepository;
import com.openuniversity.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/auth")
public class RestPasswordController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    PasswordValidation passwordValidation;


    @RequestMapping(value = "/reset-password/email", method = RequestMethod.POST)
    public ResponseEntity<?> resetUserPassword(@RequestBody ResetPasswordRequest resetPasswordRequest, @RequestParam("token")String confirmationToken) {
        boolean samePasswords = false;
        boolean validPassword = false;
        boolean validToken = false;
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token.getValidOrNot(confirmationToken).equals("Valid")){
            validToken = true;
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Token is not Valid. Please try again!"));
        }

        if(passwordValidation.validatePassword(resetPasswordRequest.getPassword())){
            validPassword = true;
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Enter a valid password!"));
        }

        if(resetPasswordRequest.getPassword().equals(resetPasswordRequest.getRetypePassword())){
            samePasswords = true;
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Passwords do not match!"));
        }

        if (token != null && samePasswords && validToken && validPassword) {
            User_Applicant tokenUserApplicant = userRepository.findByEmail(token.getUser().getEmail());
            tokenUserApplicant.setPassword(encoder.encode(resetPasswordRequest.getPassword()));
            userRepository.save(tokenUserApplicant);
            token.setValidOrNot("Invalid");
            confirmationTokenRepository.save(token);


            return ResponseEntity.ok().body(new MessageResponse("Success: Password reset successfully"));
        } else {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Password reset unsuccessful"));
        }
    }
}
