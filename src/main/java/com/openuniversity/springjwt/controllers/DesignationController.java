package com.openuniversity.springjwt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.openuniversity.springjwt.models.Designation;
import com.openuniversity.springjwt.repository.DesignationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class DesignationController {
    @Autowired
    private DesignationRepository designationRepository;

    @GetMapping("/designation")
    public List<Designation> getAllDesignation() {
        return designationRepository.findAll();
    }

    @GetMapping("/designation/{id}")
    public ResponseEntity<Designation> getDesignationById(@PathVariable(value = "id") Long designationId)
            throws ResourceNotFoundException {
        Designation designation = designationRepository.findById(designationId)
                .orElseThrow(() -> new ResourceNotFoundException("Designation not found for this id :: " + designationId));
        return ResponseEntity.ok().body(designation);
    }

    @PostMapping("/designation")
    public Designation createDesignation(@Valid @RequestBody Designation designation) {
        return designationRepository.save(designation);
    }

    @PutMapping("/designation/{id}")
    public ResponseEntity<Designation> updateDesignation(@PathVariable(value = "id") Long designationId,
                                                         @Valid @RequestBody Designation designationDetails) throws ResourceNotFoundException {
        Designation designation = designationRepository.findById(designationId)
                .orElseThrow(() -> new ResourceNotFoundException("Designation not found for this id :: " + designationId));

        designation.setDesignation(designationDetails.getDesignation());
        final Designation updatedDesignation = designationRepository.save(designation);
        return ResponseEntity.ok(updatedDesignation);
    }

    @DeleteMapping("/designation/{id}")
    public Map<String, Boolean> deleteDesignation(@PathVariable(value = "id") Long designationId)
            throws ResourceNotFoundException {
        Designation designation = designationRepository.findById(designationId)
                .orElseThrow(() -> new ResourceNotFoundException("Designation not found for this id :: " + designationId));

        designationRepository.delete(designation);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

