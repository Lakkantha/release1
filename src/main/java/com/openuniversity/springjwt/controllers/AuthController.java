package com.openuniversity.springjwt.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.openuniversity.springjwt.models.User_Applicant;
import com.openuniversity.springjwt.repository.InitialApplicantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.openuniversity.springjwt.models.ERole;
import com.openuniversity.springjwt.models.Role;
import com.openuniversity.springjwt.payload.request.LoginRequest;
import com.openuniversity.springjwt.payload.request.SignupRequest;
import com.openuniversity.springjwt.payload.response.JwtResponse;
import com.openuniversity.springjwt.payload.response.MessageResponse;
import com.openuniversity.springjwt.repository.RoleRepository;
import com.openuniversity.springjwt.repository.UserRepository;
import com.openuniversity.springjwt.security.jwt.JwtUtils;
import com.openuniversity.springjwt.security.services.UserDetailsImpl;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	InitialApplicantRepository initialApplicantRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	EmailValidation emailValidation;

	@Autowired
	PasswordValidation passwordValidation;

//	@Autowired
//	GenerateFileController generateFileController;


	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) throws ParseException {
		boolean usernameExists = false;
		boolean password = false;
		boolean phoneVerified = false;
		boolean status = false;
		Logger logger = LoggerFactory.getLogger(AuthController.class);
		User_Applicant loginUserApplicant;
		if (userRepository.existsByUsername(loginRequest.getUsername())) {
			usernameExists = true;
			loginUserApplicant = userRepository.findByUsername(loginRequest.getUsername()).orElse(null);
		} else {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: The Username has not been registered!"));
		}


		if (encoder.matches(loginRequest.getPassword(), loginUserApplicant.getPassword())) {
			password = true;
		}
		else {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Invalid password!"));
		}
		if(loginUserApplicant.getInitialApplicant().getMobileverifieid().equals("No")){
			phoneVerified = false;
		}
		else{
			phoneVerified = true;
		}

		if(loginUserApplicant.getStatus().equals("Active")){
			status = true;
		}
		else{
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: You are not an active user!"));
		}


		if (usernameExists && password && status) {

			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			GenerateFileController.GenerateFile();
			String ip = request.getRemoteAddr();
			Date date = new Date() ;
			WriteToFileController.WriteToFile(loginRequest.getUsername(), ip, date);


			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream()
					.map(item -> item.getAuthority())
					.collect(Collectors.toList());

			if(phoneVerified == false) {
				loginUserApplicant.getInitialApplicant().setMobileverifieid("Yes");
				initialApplicantRepository.save(loginUserApplicant.getInitialApplicant());
			}

			return ResponseEntity.ok(new JwtResponse(jwt,
					userDetails.getId(),
					userDetails.getUsername(),
					userDetails.getEmail(),
					userDetails.getInitialApplicant(),
					roles));
		}
		else{
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username or Password is incorrect!"));
		}

	}



}
