package com.openuniversity.springjwt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.openuniversity.springjwt.models.Institution;
import com.openuniversity.springjwt.repository.InstitutionRepository;
import com.openuniversity.springjwt.repository.QualificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;
import com.openuniversity.springjwt.models.Qualification;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class QualificationController {
    @Autowired
    private QualificationRepository qualificationRepository;
    @Autowired
    private InstitutionRepository institutionRepository;
    @Autowired
    private InstitutionController institutionController;

    @GetMapping("/qualifications")
    public List<Qualification> getAllQualifications() {
        return qualificationRepository.findAll();
    }

    @GetMapping("/qualifications/{id}")
    public ResponseEntity<Qualification> getQualificationById(@PathVariable(value = "id") Long qualificationId)
            throws ResourceNotFoundException {
        Qualification qualification = qualificationRepository.findById(qualificationId)
                .orElseThrow(() -> new ResourceNotFoundException("Qualification not found for this id :: " + qualificationId));
        return ResponseEntity.ok().body(qualification);
    }

    @PostMapping("/qualifications")
    public Qualification createQualification(@Valid @RequestBody Qualification qualification) {

//        String institution = qualification.getInstitution();
//        System.out.println(institution);
//        System.out.println(institutionRepository.getInstitutionId(institution));

        return qualificationRepository.save(qualification);
    }

    @PutMapping("/qualifications/{id}")
    public ResponseEntity<Qualification> updateQualification(@PathVariable(value = "id") Long qualificationId,
                                                   @Valid @RequestBody Qualification qualificationDetails) throws ResourceNotFoundException {
        Qualification qualification = qualificationRepository.findById(qualificationId)
                .orElseThrow(() -> new ResourceNotFoundException("Qualification not found for this id :: " + qualificationId));

        qualification.setQualification(qualificationDetails.getQualification());
        qualification.setInstitution(qualificationDetails.getInstitution());
        qualification.setEdate(qualificationDetails.getEdate());
        qualification.setCertificates(qualificationDetails.getCertificates());
        qualification.setDuration(qualificationDetails.getDuration());
        final Qualification updatedQualification = qualificationRepository.save(qualification);
        return ResponseEntity.ok(updatedQualification);
    }

    @DeleteMapping("/qualifications/{id}")
    public Map<String, Boolean> deleteQualification(@PathVariable(value = "id") Long qualificationId)
            throws ResourceNotFoundException {
        Qualification qualification = qualificationRepository.findById(qualificationId)
                .orElseThrow(() -> new ResourceNotFoundException("Qualification not found for this id :: " + qualificationId));

        qualificationRepository.delete(qualification);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
