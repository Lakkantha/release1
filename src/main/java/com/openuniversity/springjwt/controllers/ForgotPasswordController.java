package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.models.ConfirmationToken;
import com.openuniversity.springjwt.models.User_Applicant;
import com.openuniversity.springjwt.payload.request.ForgotPasswordRequest;
import com.openuniversity.springjwt.payload.response.MessageResponse;
import com.openuniversity.springjwt.repository.ConfirmationTokenRepository;
import com.openuniversity.springjwt.repository.UserRepository;

import com.openuniversity.springjwt.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.MultiValueMap;

import javax.validation.Valid;
import java.util.Random;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/auth")
public class ForgotPasswordController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailValidation emailValidation;

    @Autowired
    EmailController emailController;

    @Autowired
    SmsController smsController;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;

    @PostMapping("/forgot-password/email")
    public ResponseEntity<?> restPassword(@Valid @RequestBody ForgotPasswordRequest forgotPasswordRequest) {
        boolean emailExist = true;
        boolean isNull = false;

        if(forgotPasswordRequest.getEmail().equals("undefined")){
            isNull = true;
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Please enter your email address!"));
        }


        if (userRepository.existsByEmail(forgotPasswordRequest.getEmail())) {
            emailExist = true;
        } else {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: The email address has not been registered!"));
        }
        if (emailExist && isNull==false) {

            User_Applicant userApplicant = userRepository.findByEmail(forgotPasswordRequest.getEmail());

            ConfirmationToken confirmationToken = new ConfirmationToken(userApplicant);
            confirmationToken.setValidOrNot("Valid");

            confirmationTokenRepository.save(confirmationToken);

            String link = "http://localhost:4200/reset?token=" + confirmationToken.getConfirmationToken();

            MultiValueMap<String, String > mMap = new LinkedMultiValueMap<>();
            mMap.add("emailTo",forgotPasswordRequest.getEmail());
            mMap.add("emailFrom", "chathurajayawardane@gmail.com");
            mMap.add("emailSubject","Password rest link.");
            mMap.add("emailContent","Click the following link to reset your password\n" + link);

            emailController.sendmail(mMap);


            return ResponseEntity.ok().body(new MessageResponse("Success: The reset link has successfully been sent to your email!"));
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: The email address has not been registered!"));
        }
    }

    @PostMapping("/forgot-password/phone")
    public ResponseEntity<?> restPasswordByPhone(@Valid @RequestBody ForgotPasswordRequest forgotPasswordRequest) {
        boolean userExist = true;
        boolean isNull = false;
        boolean validConfirmationToken = false;
        User_Applicant  user_applicant;
        String phoneNum = "";
        ConfirmationToken confirmationToken;

        if(forgotPasswordRequest.getUsername().equals("undefined")){
            isNull = true;
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Please enter your username! We will send your password to your phone."));
        }



        if (userRepository.existsByUsername(forgotPasswordRequest.getUsername())) {
            userExist = true;
            user_applicant = userRepository.findByUsername(forgotPasswordRequest.getUsername()).orElse(null);
//            confirmationToken = new ConfirmationToken(user_applicant);
//            confirmationToken.setValidOrNot("Valid");
//            confirmationTokenRepository.save(confirmationToken);
        } else {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: The username has not been registered!"));
        }

        if(userExist && !isNull){

            phoneNum = user_applicant.getInitialApplicant().getMobileno();
            System.out.println(phoneNum);
            Random rnd = new Random();
            int n = 100000 + rnd.nextInt(900000);

            MultiValueMap<String, String > mMap = new LinkedMultiValueMap<>();
            mMap.add("mobileNumber",phoneNum);
            mMap.add("smsMsg", "Your new password - " + n);
            smsController.sendSms(mMap);

            System.out.println("password - " + n);

            user_applicant.setPassword(encoder.encode(Integer.toString(n)));
            userRepository.save(user_applicant);

            return ResponseEntity.ok().body(new MessageResponse("Success: The new password has successfully been sent to your mobile phone!"));
        }
        else{
            return ResponseEntity.badRequest().body(new MessageResponse("Error: The username has not been registered!"));
        }
    }
}
