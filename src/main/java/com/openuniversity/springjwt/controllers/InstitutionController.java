package com.openuniversity.springjwt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.openuniversity.springjwt.models.Institution;
import com.openuniversity.springjwt.repository.InstitutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class InstitutionController {
    @Autowired
    private InstitutionRepository institutionRepository;

    @GetMapping("/institution")
    public List<Institution> getAllInstitutions() {
        return institutionRepository.findAll();
    }

    @GetMapping("/institution/{id}")
    public ResponseEntity<Institution> getInstitutionById(@PathVariable(value = "id") Long institutionId)
            throws ResourceNotFoundException {
        Institution institution = institutionRepository.findById(institutionId)
                .orElseThrow(() -> new ResourceNotFoundException("Institution not found for this id :: " + institutionId));
        return ResponseEntity.ok().body(institution);
    }

    @PostMapping("/institution")
    public Institution createInstitution(@Valid @RequestBody Institution institution) {
        return institutionRepository.save(institution);
    }

    @PutMapping("/institution/{id}")
    public ResponseEntity<Institution> updateInstitution(@PathVariable(value = "id") Long institutionId,
                                                             @Valid @RequestBody Institution institutionDetails) throws ResourceNotFoundException {
        Institution institution = institutionRepository.findById(institutionId)
                .orElseThrow(() -> new ResourceNotFoundException("Institution not found for this id :: " + institutionId));

        institution.setInstitution(institutionDetails.getInstitution());
        final Institution updatedInstitution = institutionRepository.save(institution);
        return ResponseEntity.ok(updatedInstitution);
    }

    @DeleteMapping("/institution/{id}")
    public Map<String, Boolean> deleteInstitution(@PathVariable(value = "id") Long institutionId)
            throws ResourceNotFoundException {
        Institution institution = institutionRepository.findById(institutionId)
                .orElseThrow(() -> new ResourceNotFoundException("Institution not found for this id :: " + institutionId));

        institutionRepository.delete(institution);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

