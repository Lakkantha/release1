package com.openuniversity.springjwt.controllers;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class GenerateFileController {

    public static void GenerateFile() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        String result = String.format(
                "signInLog-%1$tY-%1$tm-%1$td.txt", cal);
        try {
            File myObj = new File("D:\\upload\\com-ousl-std-mgmt-sys-angular\\back-end\\tomcat\\logs\\" + result);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
