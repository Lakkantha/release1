package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.exception.ResourceNotFoundException;
import com.openuniversity.springjwt.models.ALCertificates;
import com.openuniversity.springjwt.models.ALQualifications;
import com.openuniversity.springjwt.models.Experience;
import com.openuniversity.springjwt.repository.ALCertificatesRepository;
import com.openuniversity.springjwt.repository.ALQualificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class ALQualificationController {

    @Autowired
    private ALQualificationRepository alRepo;

    @Autowired
    private ALCertificatesRepository alCertiRepo;

    // list all AL data
    @GetMapping("/al_data")
    public List<ALQualifications> getAllALData(){
        return alRepo.findAll();
    }

    // get AL data by id
    @GetMapping("/al_data/{id}")
    public ResponseEntity<ALQualifications> getALDataById(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        ALQualifications al = alRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));
        return ResponseEntity.ok().body(al);
    }

    // save AL data
    @PostMapping("/al_data")
    public ALQualifications createALData(@RequestBody ALQualifications alQualification) {
        return alRepo.save(alQualification);
    }

    // delete AL data
    @DeleteMapping("/al_data/{id}")
    void deleteALData(@PathVariable Long id) {
        alRepo.deleteById(id);
    }

    // update AL data
    @PutMapping("/al_data/{id}")
    public ResponseEntity<ALQualifications> updateAL(@PathVariable(value = "id") Long id,
                                                   @Valid @RequestBody ALQualifications aldata) throws ResourceNotFoundException {
        ALQualifications al = alRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

        al.setYear(aldata.getYear());
        al.setIndexNumber(aldata.getIndexNumber());
        al.setMedium(aldata.getMedium());
        al.setStream(aldata.getStream());
        al.setSubject(aldata.getSubject());
        al.setResult(aldata.getResult());

        final ALQualifications updatedAlData = alRepo.save(al);
        return ResponseEntity.ok(updatedAlData);
    }

    // list all AL Certificates data
    @GetMapping("/al_certificates")
    public List<ALCertificates> getAllALCertificatesData(){
        return alCertiRepo.findAll();
    }

    // save AL Certificates data
    @PostMapping("/al_certificates")
    public ALCertificates createALCertificates(@RequestBody ALCertificates aLCertificates) {
        return alCertiRepo.save(aLCertificates);
    }

    // delete AL Certificates data
    @DeleteMapping("/al_certificates/{id}")
    void deleteALCertificates(@PathVariable Long id) {
        alCertiRepo.deleteById(id);
    }
}
