package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.dto.InitialStudentDTO;
import com.openuniversity.springjwt.exception.NotFoundException;

import com.openuniversity.springjwt.service.custom.InitialStudentService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/v1/initialstudent")
@CrossOrigin
@RestController
public class InitialStudentController {

    @Autowired
    private InitialStudentService initialStudentService;


    @GetMapping(value = "/{applicant_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public InitialStudentDTO getInitialStudent(@PathVariable int applicant_id) {
        try {
            return initialStudentService.findstudent(applicant_id);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }


}
