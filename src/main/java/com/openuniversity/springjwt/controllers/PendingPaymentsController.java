package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.dto.PendingPaymentDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.service.custom.PendingPaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/pendingPayments")
@CrossOrigin
@RestController
public class PendingPaymentsController {

    @Autowired
    private PendingPaymentsService pendingPaymentsService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveApplicant(@RequestBody PendingPaymentDTO pendingPaymentDTO) {
        try {
            pendingPaymentsService.savePendingPayments(pendingPaymentDTO);
            return "\"" + "Sucessfully Saved"+ "\"";
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }
}
