package com.openuniversity.springjwt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.openuniversity.springjwt.models.Certificates;
import com.openuniversity.springjwt.repository.CertificateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.openuniversity.springjwt.exception.ResourceNotFoundException;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class CertificateController {
    @Autowired
    private CertificateRepository certificateRepository;

    @GetMapping("/certificates")
    public List<Certificates> getAllCertificate() {
        return certificateRepository.findAll();
    }

    @GetMapping("/certificates/{id}")
    public ResponseEntity<Certificates> getCertificatesById(@PathVariable(value = "id") Long certificateId)
            throws ResourceNotFoundException {
        Certificates certificate = certificateRepository.findById(certificateId)
                .orElseThrow(() -> new ResourceNotFoundException("Certificate not found for this id :: " + certificateId));
        return ResponseEntity.ok().body(certificate);
    }

    @PostMapping("/certificates")
    public Certificates createCertificate(@Valid @RequestBody Certificates certificate) {
        return certificateRepository.save(certificate);
    }

    @PutMapping("/certificates/{id}")
    public ResponseEntity<Certificates> updateCertificate(@PathVariable(value = "id") Long certificateId,
                                                 @Valid @RequestBody Certificates certificateDetails) throws ResourceNotFoundException {
        Certificates certificate = certificateRepository.findById(certificateId)
                .orElseThrow(() -> new ResourceNotFoundException("Certificate not found for this id :: " + certificateId));

        certificate.setCertificates(certificateDetails.getCertificates());
        final Certificates updatedCertificate = certificateRepository.save(certificate);
        return ResponseEntity.ok(updatedCertificate);
    }

    @DeleteMapping("/certificates/{id}")
    public Map<String, Boolean> deleteCompany(@PathVariable(value = "id") Long certificateId)
            throws ResourceNotFoundException {
        Certificates certificate = certificateRepository.findById(certificateId)
                .orElseThrow(() -> new ResourceNotFoundException("Certificate not found for this id :: " + certificateId));

        certificateRepository.delete(certificate);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


