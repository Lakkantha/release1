package com.openuniversity.springjwt.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.openuniversity.springjwt.repository.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;
import com.openuniversity.springjwt.models.Experience;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class WorkingExperienceController {
    @Autowired
    private ExperienceRepository experienceRepository;

    @GetMapping("/experiences")
    public List<Experience> getAllExperiences() {
        return experienceRepository.findAll();
    }

    @GetMapping("/experiences/{id}")
    public ResponseEntity<Experience> getExperienceById(@PathVariable(value = "id") Long experienceId)
            throws ResourceNotFoundException {
        Experience experience = experienceRepository.findById(experienceId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + experienceId));
        return ResponseEntity.ok().body(experience);
    }

    @PostMapping("/experiences")
    public Experience createExperience(@Valid @RequestBody Experience experience) {
        return experienceRepository.save(experience);
    }

    @PutMapping("/experiences/{id}")
    public ResponseEntity<Experience> updateExperience(@PathVariable(value = "id") Long experienceId,
                                                             @Valid @RequestBody Experience experienceDetails) throws ResourceNotFoundException {
        Experience experience = experienceRepository.findById(experienceId)
                .orElseThrow(() -> new ResourceNotFoundException("Experience not found for this id :: " + experienceId));

        experience.setCompany(experienceDetails.getCompany());
        experience.setDesignation(experienceDetails.getDesignation());
        experience.setDateFrom(experienceDetails.getDateFrom());
        experience.setCertificates(experienceDetails.getCertificates());
        experience.setDateTo(experienceDetails.getDateTo());
        final Experience updatedExperience = experienceRepository.save(experience);
        return ResponseEntity.ok(updatedExperience);
    }

    @DeleteMapping("/experiences/{id}")
    public Map<String, Boolean> deleteExperience(@PathVariable(value = "id") Long experienceId)
            throws ResourceNotFoundException {
        Experience experience = experienceRepository.findById(experienceId)
                .orElseThrow(() -> new ResourceNotFoundException("Experience not found for this id :: " + experienceId));

        experienceRepository.delete(experience);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
