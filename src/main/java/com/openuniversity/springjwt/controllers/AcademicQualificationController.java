package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.exception.ResourceNotFoundException;
import com.openuniversity.springjwt.models.*;
import com.openuniversity.springjwt.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class AcademicQualificationController {

    @Autowired
    private AcademicQualificationRepository academicRepo;

    @Autowired
    private InstitutionRepository institutionRepo;

    @Autowired
    private AcademicCertificatesRepository academicCertiRepo;

    @Autowired
    private OLSubjectsRepository olSubjectsRepo;

    // list all Academic data
    @GetMapping("/academic_data")
    public List<AcademicQualification> getAllAcademicData(){
        getAllOLSubjects();
        return academicRepo.findAll();
    }

    // list OL subjects
    public List<OLSubjects> getAllOLSubjects(){
        return olSubjectsRepo.findAll();
    }

    // save Academic data
    @PostMapping("/academic_data")
    public AcademicQualification createAcademicData(@RequestBody AcademicQualification academicQualification) {
        return academicRepo.save(academicQualification);
    }

    // update Academic data
    @PutMapping("/academic_data/{id}")
    public ResponseEntity<AcademicQualification> updateAL(@PathVariable(value = "id") Long id,
                                                     @Valid @RequestBody AcademicQualification academicdata) throws ResourceNotFoundException {
        AcademicQualification academic = academicRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

        academic.setQualificationType(academicdata.getQualificationType());
        academic.setCertificateName(academicdata.getQualificationName());
        academic.setDuration(academicdata.getDuration());
        academic.setInstitution(academicdata.getInstitution());
        academic.setEffectiveDate(academicdata.getEffectiveDate());

        final AcademicQualification updatedAcademicData = academicRepo.save(academic);
        return ResponseEntity.ok(updatedAcademicData);
    }

    // delete Academic data
    @DeleteMapping("/academic_data/{id}")
    void deleteAcademicData(@PathVariable Long id) {
        academicRepo.deleteById(id);
    }

    // list all Academic Certificates data
    @GetMapping("/academic_certificates")
    public List<AcademicCertificates> getAllAcademicCertificatesData(){
        return academicCertiRepo.findAll();
    }

    // save Academic Certificates data
    @PostMapping("/academic_certificates")
    public AcademicCertificates createAcademicCertificates(@RequestBody AcademicCertificates academicCertificates) {
        return academicCertiRepo.save(academicCertificates);
    }

    // delete Academic Certificates data
    @DeleteMapping("/academic_certificates/{id}")
    void deleteAcademicCertificates(@PathVariable Long id) {
        academicCertiRepo.deleteById(id);
    }
}
