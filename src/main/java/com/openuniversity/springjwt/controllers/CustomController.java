package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.models.CustomEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestMapping("/api/v1/custom")
@CrossOrigin
@RestController
public class CustomController {

    @PersistenceContext
    private EntityManager entityManager;

    @GetMapping(value="/userEmail/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomEntity getUser(@PathVariable int id) {
        Object result = entityManager.createNativeQuery("select u.email FROM users u where u.id=?").setParameter(1, id).getSingleResult();
        CustomEntity customEntity = new CustomEntity(result.toString());

       return customEntity;

    }
}
