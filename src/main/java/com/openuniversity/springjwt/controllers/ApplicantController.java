package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.service.custom.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/applicants")
@CrossOrigin
@RestController
public class ApplicantController {

    @Autowired
    private ApplicantService applicantService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ApplicantDTO getApplicant(@PathVariable int id) {
        try {
            return applicantService.findApplicant(id);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }

    @GetMapping
    public ResponseEntity<List<ApplicantDTO>> getAllApplicants() {
        try {
            List<ApplicantDTO> allApplicants = applicantService.findAllApplicants();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("X-Count", allApplicants.size() + "");
            return new ResponseEntity<>(allApplicants, httpHeaders, HttpStatus.OK);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveApplicant(@RequestBody ApplicantDTO applicantDTO) {
        try {
            applicantService.saveApplicant(applicantDTO);
            return "\"" + applicantDTO.getEmail() + "\"";
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteApplicant(@PathVariable int id) {
        applicantService.deleteApplicant(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void updateApplicant(@PathVariable int id,
                                @RequestBody ApplicantDTO applicant) {
        applicantService.updateApplicant(applicant);
    }
}
