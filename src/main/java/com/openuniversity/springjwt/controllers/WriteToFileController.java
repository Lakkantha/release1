package com.openuniversity.springjwt.controllers;


import java.io.*;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class WriteToFileController {

    public static void WriteToFile(String username, String ipAddress, Date date) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(Date.from(Instant.now()));
            String result = String.format(
                    "signInLog-%1$tY-%1$tm-%1$td.txt", cal);
            PrintWriter myWriter = new PrintWriter(new FileWriter("D:\\upload\\com-ousl-std-mgmt-sys-angular\\back-end\\tomcat\\logs\\" + result, true));
            myWriter.write(System.getProperty( "line.separator" ));
            myWriter.write("Username : " + username + "  IPAddress : " + ipAddress + "  loggedDate : " + date);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
