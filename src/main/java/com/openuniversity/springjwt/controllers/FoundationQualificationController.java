package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.models.FoundationQualification;
import com.openuniversity.springjwt.repository.FoundationQualificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class FoundationQualificationController {

    @Autowired
    private FoundationQualificationRepository foundRepo;

    // list all Foundation data
    @GetMapping("/foundation_data")
    public List<FoundationQualification> getAllFoundationData(){
        return foundRepo.findAll();
    }

    // save AL data
    @PostMapping("/foundation_data")
    public FoundationQualification createFoundationData(@RequestBody FoundationQualification foundationQualification) {
        return foundRepo.save(foundationQualification);
    }
}
