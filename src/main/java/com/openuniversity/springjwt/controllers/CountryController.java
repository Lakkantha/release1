package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.dto.CountryDTO;
import com.openuniversity.springjwt.service.custom.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/api/v1/countries")
@CrossOrigin
@RestController
public class CountryController {
    @Autowired
    private CountryService countryService;


    @GetMapping
    public ResponseEntity<List<CountryDTO>> getAllCountries() {
        List<CountryDTO> allCountries = countryService.findAllCountries();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("X-Count",allCountries.size() + "");

        return new ResponseEntity<>(allCountries,httpHeaders, HttpStatus.OK);
    }


}
