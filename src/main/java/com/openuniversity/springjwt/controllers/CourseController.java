package com.openuniversity.springjwt.controllers;


import com.openuniversity.springjwt.dto.CourseDTO;
import com.openuniversity.springjwt.dto.CourseSelectionDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.service.custom.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/courses")
@CrossOrigin
@RestController
public class CourseController {
    @Autowired
    private CourseService courseService;

    @GetMapping(value = "/{mobile}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CourseDTO getDefaultCourseList(@PathVariable String mobile) {

        try {
            CourseDTO defaultCourseList = courseService.getDefaultCourseList(mobile);
            return defaultCourseList;
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }


}
