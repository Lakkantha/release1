package com.openuniversity.springjwt.controllers;

import com.openuniversity.springjwt.dto.LocalApplicantDTO;
import com.openuniversity.springjwt.exception.NotFoundException;
import com.openuniversity.springjwt.models.CustomEntity;
import com.openuniversity.springjwt.service.custom.LocalApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/v1/localApplicants")
@CrossOrigin
@RestController
public class LocalApplicantController {

    @Autowired
    private LocalApplicantService localApplicantService;


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LocalApplicantDTO getLocalApplicant(@PathVariable int id) {
        try {
            return localApplicantService.findLocalApplicant(id);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }
    @GetMapping(value = "userNIC/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomEntity getNIC(@PathVariable int id) {
        try {
            String nic = localApplicantService.getNIC(id);
            return new CustomEntity(nic);
        } catch (NullPointerException e) {
            throw new NotFoundException(e);
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveLocalApplicant(@RequestBody LocalApplicantDTO applicantDTO) {
        localApplicantService.saveLocalApplicant(applicantDTO);
        return "\"" + applicantDTO.getId() + "\"";
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteForeignApplicant(@PathVariable int id) {
        localApplicantService.deleteLocalApplicant(id);
    }


}
