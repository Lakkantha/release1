package com.openuniversity.springjwt.service.custom.impl;


import com.openuniversity.springjwt.dto.ForeignApplicantDTO;
import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.models.ForeignApplicant;
import com.openuniversity.springjwt.models.InitialStudent;
import com.openuniversity.springjwt.repository.ApplicantRepository;
import com.openuniversity.springjwt.repository.ForeignApplicantRepository;
import com.openuniversity.springjwt.repository.InitialApplicantRepository;
import com.openuniversity.springjwt.repository.InitialStudentRepository;
import com.openuniversity.springjwt.service.custom.ForeignApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional
@Component
public class ForeignApplicantServiceImpl implements ForeignApplicantService {

    @Autowired
    private ForeignApplicantRepository foreignApplicantRepo;

    @Autowired
    private ApplicantRepository applicantRepo;


    @Autowired
    private InitialStudentRepository initialStudentRepo;


    @Transactional(readOnly = true)
    @Override
    public ForeignApplicantDTO findForeignApplicant(int foreignApplicantId) {
        ForeignApplicant foreignApplicant = foreignApplicantRepo.findById(foreignApplicantId).get();

        int applicantId = foreignApplicant.getApplicant().getId();
        Applicant applicant = applicantRepo.findById(applicantId).get();

        return new ForeignApplicantDTO(foreignApplicant.getId(),foreignApplicant.getFamilyName(),foreignApplicant.getFirstName(),foreignApplicant.getMiddleName(),
                foreignApplicant.getCountryOfBirth(),foreignApplicant.getPassportNo(),foreignApplicant.getPassportIssuedCountry() ,new java.sql.Date(foreignApplicant.getPassportExpirydate().getTime()),
                foreignApplicant.getEmbassyFaxNo(),foreignApplicant.getEmbassyEmailAddress(),foreignApplicant.getEmbassyContactNo(),applicant.getTitle(), applicant.getGender(), new java.sql.Date(applicant.getDateOfBirth().getTime()),applicant.getNationality(),
                applicant.getEmail(), applicant.getForiegnAddress(), applicant.getLocalAddress(),
                applicant.getMobileNoForiegn(), applicant.getMobileNoLocal());

    }

    @Override
    public void saveApplicant(ForeignApplicantDTO fAppDTO) {

        //initial Student id ganna
        InitialStudent initialStudent = initialStudentRepo.findById((long) 1).get();

        Applicant applicant = new Applicant(0,fAppDTO.getTitle(), fAppDTO.getGender(),new java.sql.Date(fAppDTO.getDateOfBirth().getTime()), fAppDTO.getNationality(),
                fAppDTO.getEmail(), fAppDTO.getForiegnAddress(), fAppDTO.getLocalAddress(),
                fAppDTO.getMobileNoForiegn(), fAppDTO.getMobileNoLocal(),
                1,new Date(),1,initialStudent);

        foreignApplicantRepo.save(new ForeignApplicant(fAppDTO.getId(),fAppDTO.getFamilyName(),fAppDTO.getFirstName(),fAppDTO.getMiddleName(),
                fAppDTO.getCountryOfBirth(),fAppDTO.getPassportNo(),fAppDTO.getPassportIssuedCountry(),new java.sql.Date(fAppDTO.getPassportExpirydate().getTime()),
                fAppDTO.getEmbassyFaxNo(),fAppDTO.getEmbassyEmailAddress(),fAppDTO.getEmbassyContactNo(),applicant,
                1, new Date(), 1));

    }

    @Override
    public void deleteApplicant(int applicantId) {
        foreignApplicantRepo.deleteById(applicantId);
    }

}
