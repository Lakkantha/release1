package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.LocalApplicantDTO;
import com.openuniversity.springjwt.service.SuperService;


public interface LocalApplicantService extends SuperService {
    LocalApplicantDTO findLocalApplicant(int applicantId) ;

    void saveLocalApplicant(LocalApplicantDTO localApplicantDTO);

    void deleteLocalApplicant(int applicantId) ;

    String getNIC(int userId);

}
