package com.openuniversity.springjwt.service.custom;


import com.openuniversity.springjwt.models.PostalCode;
import com.openuniversity.springjwt.service.SuperService;


public interface PostalCodeService extends SuperService {

    PostalCode findPostalCode(int applicantId) ;

    void deletePostalCode(int applicantId) ;

}
