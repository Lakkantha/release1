package com.openuniversity.springjwt.service.custom.impl;

import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.dto.PendingPaymentDTO;
import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.models.InitialApplicant;
import com.openuniversity.springjwt.models.PendingPayments;
import com.openuniversity.springjwt.repository.InitialApplicantRepository;
import com.openuniversity.springjwt.repository.PendingPaymentsRepository;
import com.openuniversity.springjwt.service.custom.PendingPaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Calendar;

@Transactional
@Component
public class PendingPaymentsServiceImpl implements PendingPaymentsService {

    @Autowired
    private PendingPaymentsRepository pendingPaymentsRepo;

    @Autowired
    private InitialApplicantRepository initialApplicantRepo;

    @Override
    public void savePendingPayments(PendingPaymentDTO pendingPaymentDTO) {
        long applicantId = initialApplicantRepo.getApplicantId(pendingPaymentDTO.getMobileNo());
        Date dueDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        pendingPaymentsRepo.save(new PendingPayments(0, applicantId,pendingPaymentDTO.getAmount(),dueDate,1));
    }
}
