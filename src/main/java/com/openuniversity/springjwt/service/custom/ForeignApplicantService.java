package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.ForeignApplicantDTO;
import com.openuniversity.springjwt.service.SuperService;


public interface ForeignApplicantService extends SuperService {

    ForeignApplicantDTO findForeignApplicant(int applicantId) ;

    void saveApplicant(ForeignApplicantDTO foreignApplicantDTO);

    void deleteApplicant(int applicantId) ;

}
