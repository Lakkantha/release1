package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.service.SuperService;

import java.util.List;


public interface ApplicantService extends SuperService {

    ApplicantDTO findApplicant(int applicantId) ;

    void saveApplicant(ApplicantDTO applicantDTO);

    void updateApplicant(ApplicantDTO applicantDTO);

    void deleteApplicant(int applicantId) ;

    List<ApplicantDTO> findAllApplicants();



}
