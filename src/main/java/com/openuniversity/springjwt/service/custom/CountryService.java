package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.CountryDTO;

import java.util.List;


public interface CountryService {

    List<CountryDTO> findAllCountries() ;
}
