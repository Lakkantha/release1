package com.openuniversity.springjwt.service.custom.impl;


import com.openuniversity.springjwt.dto.CountryDTO;
import com.openuniversity.springjwt.models.Country;
import com.openuniversity.springjwt.repository.CountryRepository;
import com.openuniversity.springjwt.service.custom.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class CountryServiceImpl implements CountryService {

@Autowired
private CountryRepository countryRepository;

    @Override
    public List<CountryDTO> findAllCountries() {
        List<Country> alCountries = countryRepository.findAll();
        List<CountryDTO> countryList = new ArrayList<>();
        for (Country country : alCountries) {
            countryList.add(new CountryDTO(country.getId(),country.getDescription()));
        }
        System.out.println(countryList);
        return countryList;
    }
}
