package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.CourseDTO;
import com.openuniversity.springjwt.dto.CourseSelectionDTO;
import com.openuniversity.springjwt.service.SuperService;

import java.util.List;


public interface ProgramApplicableFeeService extends SuperService {


    CourseSelectionDTO getDetailsForCourseSelection(String mobile);


}
