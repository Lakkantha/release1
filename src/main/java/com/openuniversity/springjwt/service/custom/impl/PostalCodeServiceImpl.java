package com.openuniversity.springjwt.service.custom.impl;


import com.openuniversity.springjwt.models.PostalCode;
import com.openuniversity.springjwt.repository.PostalCodeRepository;
import com.openuniversity.springjwt.service.custom.PostalCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class PostalCodeServiceImpl implements PostalCodeService {

    @Autowired
    private PostalCodeRepository postalCodeRepo;

    @Override
    public PostalCode findPostalCode(int postalId) {
        return  postalCodeRepo.findById(postalId).get();
    }

    @Override
    public void deletePostalCode(int postalId) {
        postalCodeRepo.deleteById(postalId);
    }
}
