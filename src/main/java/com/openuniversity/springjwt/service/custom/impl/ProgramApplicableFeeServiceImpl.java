package com.openuniversity.springjwt.service.custom.impl;

import com.openuniversity.springjwt.dto.ApplicableFeeDTO;
import com.openuniversity.springjwt.dto.CourseSelectionDTO;
import com.openuniversity.springjwt.models.Applicablefee;
import com.openuniversity.springjwt.models.Program;
import com.openuniversity.springjwt.models.ProgramApplicableFee;
import com.openuniversity.springjwt.repository.*;
import com.openuniversity.springjwt.service.custom.ProgramApplicableFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Transactional
@Component
public class ProgramApplicableFeeServiceImpl implements ProgramApplicableFeeService {

    @Autowired
    private InitialApplicantRepository initialApplicantRepo;

    @Autowired
    private ProgramApplicableFeeRepository programApplicableFeeRepo;


    @Override
    public CourseSelectionDTO getDetailsForCourseSelection(String mobile) {
        long applicantId = initialApplicantRepo.getApplicantId(mobile);
        Program selectedProgram = initialApplicantRepo.findById(applicantId).get().getProgramStarted().getProgram();
        long programId = selectedProgram.getProgramId();
        Double tuitionFee = 0.0;
        Date effectiveDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        String userName = initialApplicantRepo.findById(applicantId).get().getNamewithinitials();

        List<ProgramApplicableFee> programFees = programApplicableFeeRepo.getProgramFee(programId);
        List<ApplicableFeeDTO> nonTuitionFees = new ArrayList<>();
        for (ProgramApplicableFee applicableFee : programFees) {
            if (applicableFee.getApplicableFee().getId() == 4) {
                tuitionFee = applicableFee.getAmount();
                effectiveDate = applicableFee.getDateEffective();
            } else {
                nonTuitionFees.add(new ApplicableFeeDTO(applicableFee.getApplicableFee().getId(), applicableFee.getApplicableFee().getCategory(), applicableFee.getAmount()));
            }

        }
        return new CourseSelectionDTO(userName,effectiveDate,tuitionFee,nonTuitionFees);
    }
}
