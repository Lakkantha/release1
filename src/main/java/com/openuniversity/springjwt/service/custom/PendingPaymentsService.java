package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.dto.PendingPaymentDTO;
import com.openuniversity.springjwt.service.SuperService;

import java.util.List;


public interface PendingPaymentsService extends SuperService {


    void savePendingPayments(PendingPaymentDTO pendingPaymentDTO);





}
