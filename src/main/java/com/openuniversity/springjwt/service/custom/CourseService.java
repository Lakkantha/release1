package com.openuniversity.springjwt.service.custom;

import com.openuniversity.springjwt.dto.CourseDTO;
import com.openuniversity.springjwt.service.SuperService;

import java.util.List;


public interface CourseService extends SuperService {

    CourseDTO getDefaultCourseList(String mobile);


}
