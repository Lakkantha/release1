package com.openuniversity.springjwt.service.custom.impl;

import com.openuniversity.springjwt.dto.CourseCatDTO;
import com.openuniversity.springjwt.dto.CourseDTO;
import com.openuniversity.springjwt.models.Course;
import com.openuniversity.springjwt.models.Program;
import com.openuniversity.springjwt.models.ProgramCourse;
import com.openuniversity.springjwt.repository.*;
import com.openuniversity.springjwt.service.custom.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private ProgramRepository programRepo;

    @Autowired
    private ProgramCourseRepository programCourseRepo;

    @Autowired
    private InitialApplicantRepository initialApplicantRepo;

    @Override
    public CourseDTO getDefaultCourseList(String mobileNo) {
        long applicantId = initialApplicantRepo.getApplicantId(mobileNo);
        Program selectedProgram = initialApplicantRepo.findById(applicantId).get().getProgramStarted().getProgram();
        int defaultLevel = selectedProgram.getDefultLevel();
        long programId = selectedProgram.getProgramId();

        List<ProgramCourse> programCourses = programCourseRepo.getProgramCourseByProgramAndLevel(programId, defaultLevel);

        List<CourseCatDTO> cumpulsory = new ArrayList<>();
        List<CourseCatDTO> optional = new ArrayList<>();

        for (ProgramCourse programCourse : programCourses) {
            Course course = courseRepo.findById(programCourse.getCourseId().getId()).get();
            if (programCourse.getCompulsory()) {
                cumpulsory.add(new CourseCatDTO(course.getId(), course.getCode(), course.getTitle(), course.getCredit()));
            } else {
                optional.add(new CourseCatDTO(course.getId(), course.getCode(), course.getTitle(), course.getCredit()));

            }

        }
        return new CourseDTO(cumpulsory,optional);
    }
}
