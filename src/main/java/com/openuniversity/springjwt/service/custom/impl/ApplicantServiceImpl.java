package com.openuniversity.springjwt.service.custom.impl;


import com.openuniversity.springjwt.dto.ApplicantDTO;
import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.repository.ApplicantRepository;
import com.openuniversity.springjwt.service.custom.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Transactional
@Component
public class ApplicantServiceImpl implements ApplicantService {

    @Autowired
    private ApplicantRepository applicantRepo;

    @Override
    public void saveApplicant(ApplicantDTO applicantDTO) {
        applicantRepo.save(new Applicant(10, applicantDTO.getTitle(), applicantDTO.getGender(),new java.sql.Date(applicantDTO.getDateOfBirth().getTime()), applicantDTO.getNationality(), applicantDTO.getEmail(),
                applicantDTO.getForiegnAddress(), applicantDTO.getLocalAddress(), applicantDTO.getMobileNoForiegn(), applicantDTO.getMobileNoLocal(), 1, new Date(1), 1,applicantDTO.getInitialStudent()));
    }

    @Override
    public void updateApplicant(ApplicantDTO applicantDTO) {
        applicantRepo.save(new Applicant(10, applicantDTO.getTitle(), applicantDTO.getGender(),new java.sql.Date(applicantDTO.getDateOfBirth().getTime()), applicantDTO.getNationality(), applicantDTO.getEmail(),
                        applicantDTO.getForiegnAddress(), applicantDTO.getLocalAddress(), applicantDTO.getMobileNoForiegn(), applicantDTO.getMobileNoLocal(), 1, new Date(1), 1, applicantDTO.getInitialStudent()));

    }

    @Override
    public void deleteApplicant(int applicantId) {
        applicantRepo.deleteById(applicantId);
    }

    @Transactional(readOnly = true)
    @Override
    public ApplicantDTO findApplicant(int applicantId) {

        Applicant applicant = applicantRepo.findById(applicantId).get();
        return new ApplicantDTO(0,applicant.getTitle(), applicant.getGender(), applicant.getDateOfBirth(), applicant.getNationality(), applicant.getEmail(),
                applicant.getForiegnAddress(), applicant.getLocalAddress(), applicant.getMobileNoForiegn(),applicant.getMobileNoLocal(),applicant.getInitialStudent());


    }

    @Override
    public List<ApplicantDTO> findAllApplicants() {
        List<Applicant> alApplicants = applicantRepo.findAll();
        List<ApplicantDTO> applicantsList = new ArrayList<>();
        for (Applicant applicant : alApplicants) {
            applicantsList.add(new ApplicantDTO(applicant.getId(),applicant.getTitle(), applicant.getGender(),applicant.getDateOfBirth(),applicant.getNationality(),applicant.getEmail(),
                    applicant.getForiegnAddress(),applicant.getLocalAddress(),applicant.getMobileNoForiegn(),applicant.getMobileNoLocal(),applicant.getInitialStudent()));
        }
        return applicantsList;
    }


}
