package com.openuniversity.springjwt.service.custom;
import com.openuniversity.springjwt.bean.Mail;

/**
 * get the Mail object created in the EmailController
 */
public interface MailService {
    public void sendEmail(Mail mail);
}