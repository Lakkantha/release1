package com.openuniversity.springjwt.service.custom.impl;


import com.openuniversity.springjwt.dto.LocalApplicantDTO;
import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.models.InitialStudent;
import com.openuniversity.springjwt.models.LocalApplicant;
import com.openuniversity.springjwt.repository.ApplicantRepository;
import com.openuniversity.springjwt.repository.InitialStudentRepository;
import com.openuniversity.springjwt.repository.LocalApplicantRepository;
import com.openuniversity.springjwt.service.custom.LocalApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional
@Component
public class LocalApplicantServiceImpl implements LocalApplicantService {

    @Autowired
    private LocalApplicantRepository localApplicantRepo;
    @Autowired
    private ApplicantRepository applicantRepo;

    @Autowired
    private InitialStudentRepository initialStudentRepo;
    @Override
    public LocalApplicantDTO findLocalApplicant(int localApplicantId) {
        LocalApplicant localApplicant = localApplicantRepo.findById(localApplicantId).get();

        int applicantId = localApplicant.getApplicant().getId();
        Applicant applicant = applicantRepo.findById(applicantId).get();

        if(localApplicant.getBirthCertificateNo() == null){
            localApplicant.setBirthCertificateNo("");
        }


        return new LocalApplicantDTO(localApplicant.getId(), localApplicant.getNic(), localApplicant.getInitials(), localApplicant.getLastName(),
                localApplicant.getMeaningOfInitials(), localApplicant.getDistrict(), localApplicant.getDivision(),localApplicant.getAddress(),
                localApplicant.getCorrespondenceDistrict(),localApplicant.getPostalCode(),localApplicant.getMobileNo(),
                localApplicant.getDisable(),localApplicant.getReason(),localApplicant.getBirthCertificateNo(),applicant.getTitle(),applicant.getGender(),
                applicant.getDateOfBirth(),applicant.getNationality(),applicant.getEmail(),applicant.getLocalAddress(),
                applicant.getMobileNoLocal());
    }

    @Override
    public void saveLocalApplicant(LocalApplicantDTO lAppDTO) {

        InitialStudent initialStudent = initialStudentRepo.findById((long) 1).get();

        Applicant applicant = new Applicant(0,lAppDTO.getTitle(), lAppDTO.getGender(),new java.sql.Date(lAppDTO.getDateOfBirth().getTime()), lAppDTO.getNationality(),
                lAppDTO.getEmail(), "", lAppDTO.getCorrespondenceAddress(),
                "",lAppDTO.getContactNoFixed(),1,new Date(),1,initialStudent);

        if(lAppDTO.getBirthCertificateNo() == null){
            lAppDTO.setBirthCertificateNo("");
        }
        if(lAppDTO.getNic().length()==10){
            String nicOld =lAppDTO.getNic();
            String nicNew =nicOld.substring(0,8)+ nicOld.substring(8,10).toUpperCase() ;
            lAppDTO.setNic(nicNew);
        }
        localApplicantRepo.save(new LocalApplicant(lAppDTO.getId(),lAppDTO.getNic(),lAppDTO.getInitials(),lAppDTO.getLastName(),
                lAppDTO.getMeaningOfInitials(),lAppDTO.getDistrict(),lAppDTO.getDivision(),lAppDTO.getAddress(),
                lAppDTO.getCorrespondenceDistrict(),lAppDTO.getPostalCode(),lAppDTO.getMobileNo(),lAppDTO.getDisable(),
                lAppDTO.getReason(),lAppDTO.getBirthCertificateNo(),applicant,1, new Date(), 1));
    }

    @Override
    public void deleteLocalApplicant(int applicantId) {
        localApplicantRepo.deleteById(applicantId);
    }

    @Override
    public String getNIC(int userId) {
      return   localApplicantRepo.getNic(userId);
    }
}
