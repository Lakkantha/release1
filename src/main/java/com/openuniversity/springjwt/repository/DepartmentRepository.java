package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ConfirmationToken;
import com.openuniversity.springjwt.models.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}