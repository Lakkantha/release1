package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.OLSubjects;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OLSubjectsRepository extends JpaRepository<OLSubjects, Long> {
}
