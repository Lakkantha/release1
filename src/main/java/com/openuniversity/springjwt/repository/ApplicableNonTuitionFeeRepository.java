package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ApplicableNonTuitionFee;
import com.openuniversity.springjwt.models.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ApplicableNonTuitionFeeRepository extends JpaRepository<ApplicableNonTuitionFee, Long> {

}
