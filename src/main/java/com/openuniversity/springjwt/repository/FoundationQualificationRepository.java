package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.FoundationQualification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoundationQualificationRepository extends JpaRepository<FoundationQualification, Long> {
}
