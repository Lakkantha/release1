package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.AcademicCertificates;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcademicCertificatesRepository extends JpaRepository<AcademicCertificates, Long> {
}
