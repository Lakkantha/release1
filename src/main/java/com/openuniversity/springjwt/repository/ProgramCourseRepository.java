package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.ProgramCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProgramCourseRepository extends JpaRepository<ProgramCourse, Long> {

    @Query(value = "SELECT * FROM program_course pc Where pc.program_id=?1 AND pc.level=?2", nativeQuery = true)
    List<ProgramCourse> getProgramCourseByProgramAndLevel(long pId,int level);
}
