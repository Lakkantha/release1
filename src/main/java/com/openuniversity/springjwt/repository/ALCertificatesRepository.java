package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ALCertificates;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ALCertificatesRepository extends JpaRepository<ALCertificates, Long> {
}
