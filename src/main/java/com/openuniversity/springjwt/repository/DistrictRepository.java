package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District, Integer> {
}
