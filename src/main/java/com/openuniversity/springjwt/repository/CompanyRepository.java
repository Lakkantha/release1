package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long>{
}