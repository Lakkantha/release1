package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Integer> {

}
