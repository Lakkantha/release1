package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.AssociateProgram;
import com.openuniversity.springjwt.models.Program;
import com.openuniversity.springjwt.models.ProgramCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AssociateProgramRepository extends JpaRepository<AssociateProgram, Long> {

    @Query(value = "SELECT * FROM associate_program ap Where ap.prg_program_id=?1", nativeQuery = true)
    List<AssociateProgram> getAssProgramByProgramId(long pId);

}
