package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.OLCertificates;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OLCertificatesRepository extends JpaRepository<OLCertificates, Long>{
}
