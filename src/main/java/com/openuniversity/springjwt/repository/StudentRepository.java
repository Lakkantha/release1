package com.openuniversity.springjwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.openuniversity.springjwt.models.Student;

/**
 * Interact with the student table
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
