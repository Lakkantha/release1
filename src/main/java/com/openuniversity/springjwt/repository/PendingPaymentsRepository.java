package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Course;
import com.openuniversity.springjwt.models.PendingPayments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PendingPaymentsRepository extends JpaRepository<PendingPayments, Long> {

}
