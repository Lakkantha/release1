package com.openuniversity.springjwt.repository;
import com.openuniversity.springjwt.models.Certificates;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CertificateRepository extends JpaRepository<Certificates, Long> {
}
