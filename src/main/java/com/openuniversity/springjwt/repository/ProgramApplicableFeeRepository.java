package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Applicablefee;
import com.openuniversity.springjwt.models.ProgramApplicableFee;
import com.openuniversity.springjwt.models.ProgramCourse;
import com.openuniversity.springjwt.models.ProgramStarted;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ProgramApplicableFeeRepository extends JpaRepository<ProgramApplicableFee, Long> {

    @Query(value = "SELECT * FROM programapplicablefee pf Where pf.program_id=?1 ", nativeQuery = true)
    List<ProgramApplicableFee> getProgramFee(long pId);
}
