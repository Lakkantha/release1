package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.AssociateProgramAssociateCourse;
import com.openuniversity.springjwt.models.ProgramCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AscProgramAscCourseRepository extends JpaRepository<AssociateProgramAssociateCourse, Long> {

    @Query(value = "SELECT * FROM associate_program_associate_course pc Where pc.asc_program_id=?1 AND pc.level=?2 AND pc.is_prerequisites=false", nativeQuery = true)
    List<AssociateProgramAssociateCourse> getAscProgramCourseByAscProgramAndLevel(long pId, int level);
}
