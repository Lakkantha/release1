package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.AcademicQualification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcademicQualificationRepository extends JpaRepository<AcademicQualification, Long> {
}
