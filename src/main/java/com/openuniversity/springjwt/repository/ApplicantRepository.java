package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ApplicantRepository extends JpaRepository<Applicant, Integer> {

}
