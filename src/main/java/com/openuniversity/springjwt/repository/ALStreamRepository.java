package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ALStreams;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ALStreamRepository extends JpaRepository<ALStreams, Long> {
}
