package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Course;
import com.openuniversity.springjwt.models.StudentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentTypeRepository extends JpaRepository<StudentType, Long> {

}
