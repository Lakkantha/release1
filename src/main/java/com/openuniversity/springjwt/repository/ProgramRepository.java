package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.Program;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProgramRepository extends JpaRepository<Program, Long> {


}
