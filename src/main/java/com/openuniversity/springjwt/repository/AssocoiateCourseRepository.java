package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.AssociateCourse;
import com.openuniversity.springjwt.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssocoiateCourseRepository extends JpaRepository<AssociateCourse, Long> {

}
