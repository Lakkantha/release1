package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ApplicableNonTuitionFee;
import com.openuniversity.springjwt.models.ApplicableTuitionFee;
import com.openuniversity.springjwt.models.ProgramApplicableFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ApplicableTuitionFeeRepository extends JpaRepository<ApplicableTuitionFee, Long> {

    @Query(value = "SELECT atf.id from applicable_tuition_fee atf where atf.default_level =?1",nativeQuery = true)
    long getTuitionApplicableId(int defaultLevel);

    @Query(value = "SELECT  pf.amount FROM program_applicable_fee pf Where pf.program_id=?1 AND pf.table_name ='T' AND pf.applicable_fee_id=?2 LIMIT 1", nativeQuery = true)
    Double getTuitionFee(long pId,long appId);
}
