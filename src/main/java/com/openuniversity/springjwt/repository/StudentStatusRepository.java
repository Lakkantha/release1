package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.StudentStatus;
import com.openuniversity.springjwt.models.StudentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentStatusRepository extends JpaRepository<StudentStatus, Long> {

}
