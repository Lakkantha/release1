package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Department;
import com.openuniversity.springjwt.models.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Integer> {

}