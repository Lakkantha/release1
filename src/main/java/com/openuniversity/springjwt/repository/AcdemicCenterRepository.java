package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.AcademicCenter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcdemicCenterRepository extends JpaRepository<AcademicCenter, Long> {
}
