package com.openuniversity.springjwt.repository;

import java.util.Optional;

import com.openuniversity.springjwt.models.User_Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User_Applicant, Long> {
	Optional <User_Applicant> findByUsername(String username);

	Boolean existsByUsername(String username);

	User_Applicant findByEmail(String email);

	Boolean existsByEmail(String email);
}
