package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ExamMediums;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamMediumRepository extends JpaRepository<ExamMediums, Long> {
}
