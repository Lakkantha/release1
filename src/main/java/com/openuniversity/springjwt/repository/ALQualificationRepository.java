package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ALQualifications;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ALQualificationRepository extends JpaRepository<ALQualifications, Long> {
}
