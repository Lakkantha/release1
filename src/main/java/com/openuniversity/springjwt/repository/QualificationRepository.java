package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface QualificationRepository extends JpaRepository<Qualification, Long>{

}
