package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {

}
