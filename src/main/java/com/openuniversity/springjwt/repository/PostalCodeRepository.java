package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.PostalCode;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PostalCodeRepository extends JpaRepository<PostalCode, Integer> {

}
