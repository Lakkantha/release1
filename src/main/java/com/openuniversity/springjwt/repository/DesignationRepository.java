package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Designation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesignationRepository extends JpaRepository<Designation, Long> {
}
