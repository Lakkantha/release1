package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ALSubjects;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ALSubjectsRepository extends JpaRepository<ALSubjects, Long> {
}
