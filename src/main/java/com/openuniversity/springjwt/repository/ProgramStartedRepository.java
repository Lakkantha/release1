package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.models.ProgramStarted;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProgramStartedRepository extends JpaRepository<ProgramStarted, Long> {

}
