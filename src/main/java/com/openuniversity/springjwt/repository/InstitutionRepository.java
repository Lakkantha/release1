package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Institution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InstitutionRepository extends JpaRepository<Institution, Long> {

    @Query(value = "SELECT id FROM institution WHERE institution = ?", nativeQuery = true)
    int getInstitutionId(String institution);
}
