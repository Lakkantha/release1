package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Results;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultRepository extends JpaRepository<Results, Long> {
}
