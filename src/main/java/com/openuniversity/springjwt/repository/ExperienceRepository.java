package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Experience;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperienceRepository extends JpaRepository<Experience, Long> {

}
