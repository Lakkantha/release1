package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.LocalApplicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface LocalApplicantRepository extends JpaRepository<LocalApplicant, Integer> {

    @Query(value = "SELECT local_applicant.nic FROM local_applicant  WHERE local_applicant.user_last_updated=?", nativeQuery = true)
    String getNic(int user) ;


}
