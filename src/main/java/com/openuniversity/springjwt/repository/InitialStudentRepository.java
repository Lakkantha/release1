package com.openuniversity.springjwt.repository;


import com.openuniversity.springjwt.models.InitialStudent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InitialStudentRepository extends JpaRepository<InitialStudent, Long> {
}
