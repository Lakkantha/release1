package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.AssociateCourse;
import com.openuniversity.springjwt.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssociateCourseRepository extends JpaRepository<AssociateCourse, Long> {

}
