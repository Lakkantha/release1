package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.Applicant;
import com.openuniversity.springjwt.models.InitialApplicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.support.SimpleTriggerContext;


public interface InitialApplicantRepository extends JpaRepository<InitialApplicant, Long> {

    @Query(value = "SELECT id from initial_applicant where mobile_no =?1",nativeQuery = true)
    long getApplicantId(String moblie);
}
