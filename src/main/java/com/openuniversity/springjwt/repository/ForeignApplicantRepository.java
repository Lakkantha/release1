package com.openuniversity.springjwt.repository;

import com.openuniversity.springjwt.models.ForeignApplicant;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ForeignApplicantRepository extends JpaRepository<ForeignApplicant, Integer> {

}
