package com.openuniversity.springjwt.dto;

import java.util.Date;

public class ForeignApplicantDTO {

    private int id;
    private String familyName;
    private String firstName;
    private String middleName ;
    private String countryOfBirth;
    private String passportNo ;
    private String passportIssuedCountry;
    private java.sql.Date passportExpirydate;
    private String embassyFaxNo;
    private String embassyEmailAddress;
    private String embassyContactNo;

    private int title;
    private int gender;
    private java.sql.Date dateOfBirth;
    private String nationality;
    private String email;
    private String foriegnAddress;
    private String localAddress;
    private String mobileNoForiegn;
    private String mobileNoLocal;

    public ForeignApplicantDTO() {
    }

    public ForeignApplicantDTO(int id, String familyName, String firstName, String middleName, String countryOfBirth, String passportNo, String passportIssuedCountry, java.sql.Date passportExpirydate, String embassyFaxNo, String embassyEmailAddress, String embassyContactNo, int title, int gender, java.sql.Date dateOfBirth, String nationality, String email, String foriegnAddress, String localAddress, String mobileNoForiegn, String mobileNoLocal) {
        this.id = id;
        this.familyName = familyName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.countryOfBirth = countryOfBirth;
        this.passportNo = passportNo;
        this.passportIssuedCountry = passportIssuedCountry;
        this.passportExpirydate = passportExpirydate;
        this.embassyFaxNo = embassyFaxNo;
        this.embassyEmailAddress = embassyEmailAddress;
        this.embassyContactNo = embassyContactNo;
        this.title = title;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
        this.email = email;
        this.foriegnAddress = foriegnAddress;
        this.localAddress = localAddress;
        this.mobileNoForiegn = mobileNoForiegn;
        this.mobileNoLocal = mobileNoLocal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportIssuedCountry() {
        return passportIssuedCountry;
    }

    public void setPassportIssuedCountry(String passportIssuedCountry) {
        this.passportIssuedCountry = passportIssuedCountry;
    }

    public Date getPassportExpirydate() {
        return passportExpirydate;
    }

    public void setPassportExpirydate(java.sql.Date passportExpirydate) {
        this.passportExpirydate = passportExpirydate;
    }

    public String getEmbassyFaxNo() {
        return embassyFaxNo;
    }

    public void setEmbassyFaxNo(String embassyFaxNo) {
        this.embassyFaxNo = embassyFaxNo;
    }

    public String getEmbassyEmailAddress() {
        return embassyEmailAddress;
    }

    public void setEmbassyEmailAddress(String embassyEmailAddress) {
        this.embassyEmailAddress = embassyEmailAddress;
    }

    public String getEmbassyContactNo() {
        return embassyContactNo;
    }

    public void setEmbassyContactNo(String embassyContactNo) {
        this.embassyContactNo = embassyContactNo;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.sql.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForiegnAddress() {
        return foriegnAddress;
    }

    public void setForiegnAddress(String foriegnAddress) {
        this.foriegnAddress = foriegnAddress;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String localAddress) {
        this.localAddress = localAddress;
    }

    public String getMobileNoForiegn() {
        return mobileNoForiegn;
    }

    public void setMobileNoForiegn(String mobileNoForiegn) {
        this.mobileNoForiegn = mobileNoForiegn;
    }

    public String getMobileNoLocal() {
        return mobileNoLocal;
    }

    public void setMobileNoLocal(String mobileNoLocal) {
        this.mobileNoLocal = mobileNoLocal;
    }

    @Override
    public String toString() {
        return "ForeignApplicantDTO{" +
                "id=" + id +
                ", familyName='" + familyName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", passportNo='" + passportNo + '\'' +
                ", passportIssuedCountry='" + passportIssuedCountry + '\'' +
                ", passportExpirydate=" + passportExpirydate +
                ", embassyFaxNo='" + embassyFaxNo + '\'' +
                ", embassyEmailAddress='" + embassyEmailAddress + '\'' +
                ", embassyContactNo='" + embassyContactNo + '\'' +
                ", title=" + title +
                ", gender=" + gender +
                ", dateOfBirth=" + dateOfBirth +
                ", nationality='" + nationality + '\'' +
                ", email='" + email + '\'' +
                ", foriegnAddress='" + foriegnAddress + '\'' +
                ", localAddress='" + localAddress + '\'' +
                ", mobileNoForiegn='" + mobileNoForiegn + '\'' +
                ", mobileNoLocal='" + mobileNoLocal + '\'' +
                '}';
    }
}
