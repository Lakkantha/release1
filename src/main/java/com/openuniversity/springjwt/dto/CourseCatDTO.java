package com.openuniversity.springjwt.dto;

public class CourseCatDTO {
    private long id;
    private String code;
    private String title;
    private int credit;

    public CourseCatDTO() {
    }

    public CourseCatDTO(long id, String code, String title, int credit) {
        this.id = id;
        this.code = code;
        this.title = title;
        this.credit = credit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CourseDTO{" +
                "id=" + getId() +
                ", code='" + getCode() + '\'' +
                ", title='" + getTitle() + '\'' +
                ", credit=" + getCredit() +
                '}';
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }


}
