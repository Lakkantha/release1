package com.openuniversity.springjwt.dto;

public class PendingPaymentDTO {
    private String mobileNo;
    private Double amount;

    public PendingPaymentDTO() {
    }

    public PendingPaymentDTO(String mobileNo, Double amount) {
        this.mobileNo = mobileNo;
        this.amount = amount;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PendingPaymentDTO{" +
                "mobileNo='" + mobileNo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
