package com.openuniversity.springjwt.dto;

public class UserDTO {
    private long id ;
    private String email;
    private String userName;

    public UserDTO() {
    }

    public UserDTO(long id, String email, String userName) {
        this.id = id;
        this.email = email;
        this.userName = userName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
