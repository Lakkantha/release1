package com.openuniversity.springjwt.dto;

import java.util.Date;

public class LocalApplicantDTO {

    private int id;
    private String nic;
    private String initials;
    private String lastName;
    private String meaningOfInitials;
    private int district;
    private int division;
    private String address;
    private int correspondenceDistrict;
    private int postalCode;
    private String mobileNo;
    private int disable;
    private String reason;
    private String     birthCertificateNo;

    private int title;
    private int gender;
    private Date dateOfBirth;
    private String nationality;
    private String email;
    private String correspondenceAddress;;
    private String contactNoFixed;


    public LocalApplicantDTO() {
    }

    public LocalApplicantDTO(int id, String nic, String initials, String lastName, String meaningOfInitials, int district, int division, String address, int correspondenceDistrict, int postalCode, String mobileNo, int disable, String reason, String birthCertificateNo, int title, int gender, Date dateOfBirth, String nationality, String email, String correspondenceAddress, String contactNoFixed) {
        this.id = id;
        this.nic = nic;
        this.initials = initials;
        this.lastName = lastName;
        this.meaningOfInitials = meaningOfInitials;
        this.district = district;
        this.division = division;
        this.address = address;
        this.correspondenceDistrict = correspondenceDistrict;
        this.postalCode = postalCode;
        this.mobileNo = mobileNo;
        this.disable = disable;
        this.reason = reason;
        this.birthCertificateNo = birthCertificateNo;
        this.title = title;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
        this.email = email;
        this.correspondenceAddress = correspondenceAddress;
        this.contactNoFixed = contactNoFixed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMeaningOfInitials() {
        return meaningOfInitials;
    }

    public void setMeaningOfInitials(String meaningOfInitials) {
        this.meaningOfInitials = meaningOfInitials;
    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCorrespondenceDistrict() {
        return correspondenceDistrict;
    }

    public void setCorrespondenceDistrict(int correspondenceDistrict) {
        this.correspondenceDistrict = correspondenceDistrict;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


    public int getDisable() {
        return disable;
    }

    public void setDisable(int disable) {
        this.disable = disable;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getBirthCertificateNo() {
        return birthCertificateNo;
    }

    public void setBirthCertificateNo(String birthCertificateNo) {
        this.birthCertificateNo = birthCertificateNo;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getContactNoFixed() {
        return contactNoFixed;
    }

    public void setContactNoFixed(String contactNoFixed) {
        this.contactNoFixed = contactNoFixed;
    }

    @Override
    public String toString() {
        return "LocalApplicantDTO{" +
                "id=" + id +
                ", nic='" + nic + '\'' +
                ", initials='" + initials + '\'' +
                ", lastName='" + lastName + '\'' +
                ", meaningOfInitials='" + meaningOfInitials + '\'' +
                ", district=" + district +
                ", division=" + division +
                ", address='" + address + '\'' +
                ", correspondenceDistrict=" + correspondenceDistrict +
                ", postalCode=" + postalCode +
                ", mobileNo=" + mobileNo +
                ", disable='" + disable + '\'' +
                ", reason='" + reason + '\'' +
                ", birthCertificateNo=" + birthCertificateNo +
                ", title=" + title +
                ", gender=" + gender +
                ", dateOfBirth=" + dateOfBirth +
                ", nationality='" + nationality + '\'' +
                ", email='" + email + '\'' +
                ", correspondenceAddress='" + correspondenceAddress + '\'' +
                ", contactNoFixed='" + contactNoFixed + '\'' +
                '}';
    }
}
