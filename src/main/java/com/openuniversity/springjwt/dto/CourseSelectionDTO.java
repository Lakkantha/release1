package com.openuniversity.springjwt.dto;

import java.sql.Date;
import java.util.List;

public class CourseSelectionDTO {
    private String user;
    private Date  effectiveDate;
    private Double tutionFee;
    private List<ApplicableFeeDTO> nonTutionFees;

    public CourseSelectionDTO() {
    }

    public CourseSelectionDTO(String user, Date effectiveDate, Double tutionFee, List<ApplicableFeeDTO> nonTutionFees) {
        this.user = user;
        this.effectiveDate = effectiveDate;
        this.tutionFee = tutionFee;
        this.nonTutionFees = nonTutionFees;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Double getTutionFee() {
        return tutionFee;
    }

    public void setTutionFee(Double tutionFee) {
        this.tutionFee = tutionFee;
    }

    public List<ApplicableFeeDTO> getNonTutionFees() {
        return nonTutionFees;
    }

    public void setNonTutionFees(List<ApplicableFeeDTO> nonTutionFees) {
        this.nonTutionFees = nonTutionFees;
    }
}
