package com.openuniversity.springjwt.dto;

import com.openuniversity.springjwt.models.InitialStudent;

import java.util.Date;

public class ApplicantDTO {
        private int id;
        private int title;
        private int gender;
        private Date dateOfBirth;
        private String nationality;
        private String email;
        private String foriegnAddress;
        private String localAddress;
        private String mobileNoForiegn;
        private String mobileNoLocal;
        private InitialStudent initialStudent;



    public ApplicantDTO(int id, int title, int gender, java.sql.Date dateOfBirth, String nationality, String email, String foriegnAddress, String localAddress, String mobileNoForiegn, String mobileNoLocal, InitialStudent initialStudent) {



        this.id = id;
        this.title = title;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
        this.email = email;
        this.foriegnAddress = foriegnAddress;
        this.localAddress = localAddress;
        this.mobileNoForiegn = mobileNoForiegn;
        this.mobileNoLocal = mobileNoLocal;
        this.initialStudent = initialStudent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForiegnAddress() {
        return foriegnAddress;
    }

    public void setForiegnAddress(String foriegnAddress) {
        this.foriegnAddress = foriegnAddress;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String localAddress) {
        this.localAddress = localAddress;
    }

    public String getMobileNoForiegn() {
        return mobileNoForiegn;
    }

    public void setMobileNoForiegn(String mobileNoForiegn) {
        this.mobileNoForiegn = mobileNoForiegn;
    }

    public String getMobileNoLocal() {
        return mobileNoLocal;
    }

    public void setMobileNoLocal(String mobileNoLocal) {
        this.mobileNoLocal = mobileNoLocal;
    }

    public InitialStudent getInitialStudent() {
        return initialStudent;
    }

    public void setInitialStudent(InitialStudent initialStudent) {
        this.initialStudent = initialStudent;
    }
}
