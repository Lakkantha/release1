package com.openuniversity.springjwt.dto;

import java.util.List;

public class CourseDTO {
    private List<CourseCatDTO> cumpulsoryCourseList;
    private List<CourseCatDTO> optionalCourseList;

    public CourseDTO() {
    }

    public CourseDTO(List<CourseCatDTO> cumpulsoryCourseList, List<CourseCatDTO> optionalCourseList) {
        this.setCumpulsoryCourseList(cumpulsoryCourseList);
        this.setOptionalCourseList(optionalCourseList);
    }

    public List<CourseCatDTO> getOptionalCourseList() {
        return optionalCourseList;
    }

    public void setOptionalCourseList(List<CourseCatDTO> optionalCourseList) {
        this.optionalCourseList = optionalCourseList;
    }

    public List<CourseCatDTO> getCumpulsoryCourseList() {
        return cumpulsoryCourseList;
    }

    public void setCumpulsoryCourseList(List<CourseCatDTO> cumpulsoryCourseList) {
        this.cumpulsoryCourseList = cumpulsoryCourseList;
    }
}
