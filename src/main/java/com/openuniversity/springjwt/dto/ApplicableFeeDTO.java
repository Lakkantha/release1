package com.openuniversity.springjwt.dto;

public class ApplicableFeeDTO {
    private long id;
    private String description;
    private Double fee;

    public ApplicableFeeDTO() {
    }

    public ApplicableFeeDTO(long id, String description, Double fee) {
        this.id = id;
        this.description = description;
        this.fee = fee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        return "ApplicableFeeDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fee=" + fee +
                '}';
    }
}
