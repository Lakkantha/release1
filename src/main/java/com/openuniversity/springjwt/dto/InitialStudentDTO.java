package com.openuniversity.springjwt.dto;

import com.openuniversity.springjwt.models.*;

import javax.persistence.*;

public class InitialStudentDTO {


    private int id;
    private int programstartedid;
    private int streamid;
    private int specializationid;
    private int academiccenterid;
    private int admincenterid;
    private int med_mediumId;
    private int programentryqualificationid;
    private String nic;
    private String correspondanceaddress;

    private String namewithinitials;
    private String mobileno;
    private String faxno;

    private String email;
    private String mobileverifieid;
    private String emailverifiedid;
    private int applicantype_id;
    private int applicant_id;

    public InitialStudentDTO() {
    }

    public InitialStudentDTO(int id, int programstartedid, int streamid, int specializationid, int academiccenterid, int admincenterid, int med_mediumId, int programentryqualificationid, String nic, String correspondanceaddress, String namewithinitials, String mobileno, String faxno, String email, String mobileverifieid, String emailverifiedid, int applicantype_id, int applicant_id) {
        this.id = id;
        this.programstartedid = programstartedid;
        this.streamid = streamid;
        this.specializationid = specializationid;
        this.academiccenterid = academiccenterid;
        this.admincenterid = admincenterid;
        this.med_mediumId = med_mediumId;
        this.programentryqualificationid = programentryqualificationid;
        this.nic = nic;
        this.correspondanceaddress = correspondanceaddress;
        this.namewithinitials = namewithinitials;
        this.mobileno = mobileno;
        this.faxno = faxno;
        this.email = email;
        this.mobileverifieid = mobileverifieid;
        this.emailverifiedid = emailverifiedid;
        this.applicantype_id = applicantype_id;
        this.applicant_id = applicant_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProgramstartedid() {
        return programstartedid;
    }

    public void setProgramstartedid(int programstartedid) {
        this.programstartedid = programstartedid;
    }

    public int getStreamid() {
        return streamid;
    }

    public void setStreamid(int streamid) {
        this.streamid = streamid;
    }

    public int getSpecializationid() {
        return specializationid;
    }

    public void setSpecializationid(int specializationid) {
        this.specializationid = specializationid;
    }

    public int getAcademiccenterid() {
        return academiccenterid;
    }

    public void setAcademiccenterid(int academiccenterid) {
        this.academiccenterid = academiccenterid;
    }

    public int getAdmincenterid() {
        return admincenterid;
    }

    public void setAdmincenterid(int admincenterid) {
        this.admincenterid = admincenterid;
    }

    public int getMed_mediumId() {
        return med_mediumId;
    }

    public void setMed_mediumId(int med_mediumId) {
        this.med_mediumId = med_mediumId;
    }

    public int getProgramentryqualificationid() {
        return programentryqualificationid;
    }

    public void setProgramentryqualificationid(int programentryqualificationid) {
        this.programentryqualificationid = programentryqualificationid;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getCorrespondanceaddress() {
        return correspondanceaddress;
    }

    public void setCorrespondanceaddress(String correspondanceaddress) {
        this.correspondanceaddress = correspondanceaddress;
    }

    public String getNamewithinitials() {
        return namewithinitials;
    }

    public void setNamewithinitials(String namewithinitials) {
        this.namewithinitials = namewithinitials;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getFaxno() {
        return faxno;
    }

    public void setFaxno(String faxno) {
        this.faxno = faxno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileverifieid() {
        return mobileverifieid;
    }

    public void setMobileverifieid(String mobileverifieid) {
        this.mobileverifieid = mobileverifieid;
    }

    public String getEmailverifiedid() {
        return emailverifiedid;
    }

    public void setEmailverifiedid(String emailverifiedid) {
        this.emailverifiedid = emailverifiedid;
    }

    public int getApplicantype_id() {
        return applicantype_id;
    }

    public void setApplicantype_id(int applicantype_id) {
        this.applicantype_id = applicantype_id;
    }

    public int getApplicant_id() {
        return applicant_id;
    }

    public void setApplicant_id(int applicant_id) {
        this.applicant_id = applicant_id;
    }


    @Override
    public String toString() {
        return "InitialStudentDTO{" +
                "id=" + id +
                ", programstartedid=" + programstartedid +
                ", streamid=" + streamid +
                ", specializationid=" + specializationid +
                ", academiccenterid=" + academiccenterid +
                ", admincenterid=" + admincenterid +
                ", med_mediumId=" + med_mediumId +
                ", programentryqualificationid=" + programentryqualificationid +
                ", nic='" + nic + '\'' +
                ", correspondanceaddress='" + correspondanceaddress + '\'' +
                ", namewithinitials='" + namewithinitials + '\'' +
                ", mobileno='" + mobileno + '\'' +
                ", faxno='" + faxno + '\'' +
                ", email='" + email + '\'' +
                ", mobileverifieid='" + mobileverifieid + '\'' +
                ", emailverifiedid='" + emailverifiedid + '\'' +
                ", applicantype_id=" + applicantype_id +
                ", applicant_id=" + applicant_id +
                '}';
    }
}
