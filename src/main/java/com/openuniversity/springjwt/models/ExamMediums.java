package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "medium")
public class ExamMediums {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "medium")
    private String medium;

    public ExamMediums() {
    }

    public ExamMediums(Long id, String medium) {
        this.id = id;
        this.medium = medium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
