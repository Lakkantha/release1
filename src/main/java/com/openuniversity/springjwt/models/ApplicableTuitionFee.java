package com.openuniversity.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ApplicableTuitionFee implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int defaultLevel;
    private String description;

    public ApplicableTuitionFee() {
    }

    public ApplicableTuitionFee(long id, int defaultLevel, String description) {
        this.id = id;
        this.defaultLevel = defaultLevel;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDefaultLevel() {
        return defaultLevel;
    }

    public void setDefaultLevel(int defaultLevel) {
        this.defaultLevel = defaultLevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ApplicableTuitionFee{" +
                "id=" + id +
                ", defaultLevel=" + defaultLevel +
                ", description='" + description + '\'' +
                '}';
    }
}
