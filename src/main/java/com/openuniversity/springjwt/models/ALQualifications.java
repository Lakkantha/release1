package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "alcertified")
public class ALQualifications {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "year")
    private int year;

    @Column(name = "index_number")
    private String indexNumber;

    @Column(name = "medium")
    private String medium;

    @Column(name = "stream")
    private String stream;

    @Column(name = "subject")
    private String subject;

    @Column(name = "result")
    private String result;

    public ALQualifications() {
    }

    public ALQualifications(Long id, int year, String indexNumber, String medium, String stream, String subject, String result) {
        this.id = id;
        this.year = year;
        this.indexNumber = indexNumber;
        this.medium = medium;
        this.stream = stream;
        this.subject = subject;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
