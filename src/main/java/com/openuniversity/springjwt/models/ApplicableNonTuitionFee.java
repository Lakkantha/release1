package com.openuniversity.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ApplicableNonTuitionFee implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private Boolean isRefundable;

    public ApplicableNonTuitionFee() {
    }

    public ApplicableNonTuitionFee(long id, String description, Boolean isRefundable) {
        this.id = id;
        this.description = description;
        this.isRefundable = isRefundable;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getRefundable() {
        return isRefundable;
    }

    public void setRefundable(Boolean refundable) {
        isRefundable = refundable;
    }

    @Override
    public String toString() {
        return "ApplicableNonTuitionFee{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", isRefundable=" + isRefundable +
                '}';
    }
}
