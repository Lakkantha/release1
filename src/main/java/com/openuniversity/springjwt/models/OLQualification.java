package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table (name = "olcertified")
public class OLQualification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "year")
    private int year;

    @Column(name = "index_number")
    private String indexNumber;

    @Column(name = "medium")
    private String medium;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "subject_id", referencedColumnName = "id", nullable = false)
    private OLSubjects olSubjects;

    @Column(name = "result")
    private String result;


    public OLQualification() {

    }

    public OLQualification(Long id, int year, String indexNumber, String medium, OLSubjects olSubjects, String result) {
        this.id = id;
        this.year = year;
        this.indexNumber = indexNumber;
        this.medium = medium;
        this.olSubjects = olSubjects;
        this.result = result;
    }

    // getter and setter methods
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public OLSubjects getOlSubjects() {
        return olSubjects;
    }

    public void setOlSubjects(OLSubjects olSubjects) {
        this.olSubjects = olSubjects;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
