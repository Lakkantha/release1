package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class LocalApplicant implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String nic;
    @Column(nullable = false)
    private String initials;
    @Column(nullable = false)
    private String lastName;
    @Column(name = "nameDenotedByInitials",nullable = false)
    private String meaningOfInitials;

    @Column(name = "districtId",nullable = false)
    private
    int district;
    @Column(name = "divisionalSecretariatId",nullable = false)
    private int division;

    @Column(nullable = false)
    private String address;

    @Column(name = "correspondance_district_id",nullable = false)
    private
    int correspondenceDistrict;

    @Column(nullable = false)
    private int postalCode;
    @Column(nullable = false)
    private String mobileNo;
    private int disable;
    private String reason;
    private String birthCertificateNo;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "applicant_id", referencedColumnName = "id", nullable = false)
    private Applicant applicant;

    private int archived;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;

    private int userLastUpdated;

    public LocalApplicant() {
    }

    public LocalApplicant(int id, String nic, String initials, String lastName, String meaningOfInitials, int district, int division, String address, int correspondenceDistrict, int postalCode, String mobileNo, int disable, String reason,String birthCertificateNo, Applicant applicant, int archived, Date userCreatedDate, int userLastUpdated) {
        this.setId(id);
        this.setNic(nic);
        this.setInitials(initials);
        this.setLastName(lastName);
        this.setMeaningOfInitials(meaningOfInitials);
        this.setDistrict(district);
        this.setDivision(division);
        this.setAddress(address);
        this.setCorrespondenceDistrict(correspondenceDistrict);
        this.setPostalCode(postalCode);
        this.setMobileNo(mobileNo);
        this.setDisable(disable);
        this.setReason(reason);
        this.setBirthCertificateNo(birthCertificateNo);
        this.setApplicant(applicant);
        this.setArchived(archived);
        this.setUserCreatedDate(userCreatedDate);
        this.setUserLastUpdated(userLastUpdated);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMeaningOfInitials() {
        return meaningOfInitials;
    }

    public void setMeaningOfInitials(String meaningOfInitials) {
        this.meaningOfInitials = meaningOfInitials;
    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCorrespondenceDistrict() {
        return correspondenceDistrict;
    }

    public void setCorrespondenceDistrict(int correspondenceDistrict) {
        this.correspondenceDistrict = correspondenceDistrict;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String  mobileNo) {
        this.mobileNo = mobileNo;
    }


    public int getDisable() {
        return disable;
    }

    public void setDisable(int disable) {
        this.disable = disable;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getBirthCertificateNo() {
        return birthCertificateNo;
    }

    public void setBirthCertificateNo(String birthCertificateNo) {
        this.birthCertificateNo = birthCertificateNo;
    }
}
