package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "pendingpayments")
public class PendingPayments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long studentId;
    private double dueAmount;
    private Date dateToBe;
    private long transactionId;


    public PendingPayments() {
    }

    public PendingPayments(long id, long studentId, double dueAmount, Date dateToBe, long transactionId) {
        this.id = id;
        this.studentId = studentId;
        this.dueAmount = dueAmount;
        this.dateToBe = dateToBe;
        this.transactionId = transactionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(double dueAmount) {
        this.dueAmount = dueAmount;
    }

    public Date getDateToBe() {
        return dateToBe;
    }

    public void setDateToBe(Date dateToBe) {
        this.dateToBe = dateToBe;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "PendingPayments{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", dueAmount=" + dueAmount +
                ", dateToBe=" + dateToBe +
                ", transactionId=" + transactionId +
                '}';
    }
}
