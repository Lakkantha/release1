package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.security.cert.Certificate;
import java.util.Date;

@Entity
@Table(name = "professionalqualification")
public class Qualification {

    private long id;
    private String qualification;
    private Date edate;
    private int duration;
    private String institution;
    private String certificates;

//    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
//    @JoinColumn(name = "institution_id", referencedColumnName = "id", nullable = false)
//    private Institution institution;
//
//    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
//    @JoinColumn(name = "certificate_id", referencedColumnName = "id", nullable = false)
//    private Certificates certificates;

    public Qualification() {
    }

    public Qualification(long id, String qualification, Date edate, int duration, String institution, String certificates) {
        this.id = id;
        this.qualification = qualification;
        this.edate = edate;
        this.duration = duration;
        this.institution = institution;
        this.certificates = certificates;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "certificates", nullable = false)
    public String getCertificates() {
        return certificates;
    }
    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    @Column(name = "qualification", nullable = false)
    public String getQualification() { return qualification; }
    public void setQualification(String qualification) { this.qualification = qualification; }

    @Column(name = "institution", nullable = false)
    public String getInstitution() { return institution; }
    public void setInstitution(String institution) { this.institution = institution; }

    @Column(name = "edate", nullable = false)
    public Date getEdate() { return edate; }
    public void setEdate(Date edate) { this.edate = edate; }

    @Column(name = "duration", nullable = false)
    public int getDuration() { return duration; }
    public void setDuration(int duration) { this.duration = duration; }

    @Override
    public String toString() {
        return "Qualification{" +
                "id=" + id +
                ", qualification='" + qualification + '\'' +
                ", edate=" + edate +
                ", duration=" + duration +
                ", institution=" + institution +
                ", certificates=" + certificates +
                '}';
    }
}
