package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name="  rg_p_deliveryMode ")
public class DeliveryMode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dlm_deliveryModeId")
    private long id;

    @Column(name = "dlm_description")
    private  String description;

     public DeliveryMode(){

    }
    public DeliveryMode(long id, String description) {
        this.id = id;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
