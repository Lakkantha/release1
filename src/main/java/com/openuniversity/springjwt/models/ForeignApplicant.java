package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ForeignApplicant implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String familyName;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String middleName ;
    @Column(nullable = false)
    private String countryOfBirth;
    @Column(nullable = false)
    private String passportNo ;
    @Column(nullable = false)
    private String passportIssuedCountry;
    @Column(nullable = false)
    private java.sql.Date passportExpirydate;
    private String embassyFaxNo;
    private String embassyEmailAddress;
    private String embassyContactNo;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "applicant_id", referencedColumnName = "id", nullable = false)
    private Applicant applicant;

    //uploadImage : any;

    private int archived;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;

    private int userLastUpdated;


    public ForeignApplicant() {
    }

    public ForeignApplicant(int id, String familyName, String firstName, String middleName, String countryOfBirth, String passportNo, String passportIssuedCountry, java.sql.Date passportExpirydate, String embassyFaxNo, String embassyEmailAddress, String embassyContactNo, Applicant applicant, int archived, Date userCreatedDate, int userLastUpdated) {
        this.id = id;
        this.familyName = familyName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.countryOfBirth = countryOfBirth;
        this.passportNo = passportNo;
        this.passportIssuedCountry = passportIssuedCountry;
        this.passportExpirydate = passportExpirydate;
        this.embassyFaxNo = embassyFaxNo;
        this.embassyEmailAddress = embassyEmailAddress;
        this.embassyContactNo = embassyContactNo;
        this.applicant = applicant;
        this.archived = archived;
        this.userCreatedDate = userCreatedDate;
        this.userLastUpdated = userLastUpdated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportIssuedCountry() {
        return passportIssuedCountry;
    }

    public void setPassportIssuedCountry(String passportIssuedCountry) {
        this.passportIssuedCountry = passportIssuedCountry;
    }

    public Date getPassportExpirydate() {
        return passportExpirydate;
    }

    public void setPassportExpirydate(java.sql.Date passportExpirydate) {
        this.passportExpirydate = passportExpirydate;
    }

    public String getEmbassyFaxNo() {
        return embassyFaxNo;
    }

    public void setEmbassyFaxNo(String embassyFaxNo) {
        this.embassyFaxNo = embassyFaxNo;
    }

    public String getEmbassyEmailAddress() {
        return embassyEmailAddress;
    }

    public void setEmbassyEmailAddress(String embassyEmailAddress) {
        this.embassyEmailAddress = embassyEmailAddress;
    }

    public String getEmbassyContactNo() {
        return embassyContactNo;
    }

    public void setEmbassyContactNo(String embassyContactNo) {
        this.embassyContactNo = embassyContactNo;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
}
