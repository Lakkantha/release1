package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ProgramCourse implements SuperEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "program_id", referencedColumnName = "programId", nullable = false)
    private
    Program programId;
    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
    private
    Course courseId;
    private int level;
    private Boolean isCompulsory;
    private Boolean isPrerequisites;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;

    private int userLastUpdated;

    public ProgramCourse() {
    }

    public ProgramCourse(long id, Program programId, Course courseId, int level, Boolean isCompulsory, Boolean isPrerequisites, Date userCreatedDate, int userLastUpdated) {
        this.id = id;
        this.programId = programId;
        this.courseId = courseId;
        this.level = level;
        this.isCompulsory = isCompulsory;
        this.isPrerequisites = isPrerequisites;
        this.userCreatedDate = userCreatedDate;
        this.userLastUpdated = userLastUpdated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Program getProgramId() {
        return programId;
    }

    public void setProgramId(Program programId) {
        this.programId = programId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Boolean getCompulsory() {
        return isCompulsory;
    }

    public void setCompulsory(Boolean compulsory) {
        isCompulsory = compulsory;
    }

    public Boolean getPrerequisites() {
        return isPrerequisites;
    }

    public void setPrerequisites(Boolean prerequisites) {
        isPrerequisites = prerequisites;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
}
