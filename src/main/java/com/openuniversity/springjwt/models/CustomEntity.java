package com.openuniversity.springjwt.models;

public class CustomEntity implements SuperEntity {
    private String adminCenterName;
    private String email;
    private String nic;

    public CustomEntity() {
    }
    public CustomEntity(String email){
        this.email =email;
    }

    public String getAdminCenterName() {
        return adminCenterName;
    }

    public void setAdminCenterName(String adminCenterName) {
        this.adminCenterName = adminCenterName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }
}
