package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "programapplicablefee")
public class ProgramApplicableFee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "program_id", referencedColumnName = "programId", nullable = false)
    private Program program;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "applicable_fee_id", referencedColumnName = "id", nullable = false)
    private Applicablefee applicableFee;

    private double amount;
    private boolean status;
    private Date dateEffective;

    public ProgramApplicableFee() {
    }

    public ProgramApplicableFee(long id, Program program, Applicablefee applicableFee, double amount, boolean status, Date dateEffective) {
        this.id = id;
        this.program = program;
        this.applicableFee = applicableFee;
        this.amount = amount;
        this.status = status;
        this.dateEffective = dateEffective;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public Applicablefee getApplicableFee() {
        return applicableFee;
    }

    public void setApplicableFee(Applicablefee applicableFee) {
        this.applicableFee = applicableFee;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    @Override
    public String toString() {
        return "ProgramApplicableFee{" +
                "id=" + id +
                ", program=" + program +
                ", applicablrFeeId=" + applicableFee +
                ", amount=" + amount +
                ", status=" + status +
                ", dateEffective=" + dateEffective +
                '}';
    }
}
