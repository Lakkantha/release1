package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "academiccertificates")
public class AcademicCertificates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "certifiate_name")
    private String certificateName;

    public AcademicCertificates() {
    }

    public AcademicCertificates(Long id, String certificateName) {
        this.id = id;
        this.certificateName = certificateName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }
}
