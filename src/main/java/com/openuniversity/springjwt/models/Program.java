package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.security.PrivateKey;

@Entity
@Table(name = "program")
public class Program {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long programId;

    @Column(unique = true)
    private String code;

    private String title;

    @ManyToOne
    @JoinColumn(name = "facultyId", referencedColumnName="id", nullable = false)
    private Faculty facultyId;

    @ManyToOne
    @JoinColumn(name = "department", referencedColumnName="departmentId", nullable = false)
    private Department department;

    private int StudyModeId;
    private String noOflevels;
    private float totalMinimumCredit;
    private int ProgramOfstudyId;
    private String duration;
    private String introduceYear;
    private String startOfacademicYear;
    private boolean hasDepartmentFacultyApproval;
    private boolean hasGraduationRequest;
    private boolean isDisciplineApplicable;
    private float installmentRate;
    private String elegibilityValidityPeriod;
    private int DefultLevel;
    private boolean isExitPoint;
    private boolean isFlatRateForNewRegistration;
    private float flatRateForNewRegistartion;
    private boolean isFlatRateForReRegistration;
    private float flatRateForReRegistartion;
    private float lateFee;
    private int PararelProgramId;
    private boolean isSpecilizationApplicable;
    private String   validityPeriod;
    private float ExemptedMarks;
    private int ExemptedGradeId;
    private boolean isdefineRateForReRegirstration;
    private float defineRateForReRegirstration;
    private boolean  hasLocalStudent;
    private boolean hasForiegnStudent;
    private boolean hasBothStudents;
    private int OfferedBy;
    private  int OuslUnitId;
    private boolean isStreamApplicable;
    private  boolean isNoneapplicable;
    private int DeliveryModeId;
    private int AcademicYearID;
    private  boolean      hasEntryExam;
    private  boolean HasCounselling;
    private int CounsellingMethodId;
    private int CounsellingRecListId;


 public Program(){


 }

    public Program(long programId, String code, String title, Faculty facultyId, Department department, int studyModeId, String noOflevels, float totalMinimumCredit, int programOfstudyId, String duration, String introduceYear, String startOfacademicYear, boolean hasDepartmentFacultyApproval, boolean hasGraduationRequest, boolean isDisciplineApplicable, float installmentRate, String elegibilityValidityPeriod, int defultLevel, boolean isExitPoint, boolean isFlatRateForNewRegistration, float flatRateForNewRegistartion, boolean isFlatRateForReRegistration, float flatRateForReRegistartion, float lateFee, int pararelProgramId, boolean isSpecilizationApplicable, String validityPeriod, float exemptedMarks, int exemptedGradeId, boolean isdefineRateForReRegirstration, float defineRateForReRegirstration, boolean hasLocalStudent, boolean hasForiegnStudent, boolean hasBothStudents, int offeredBy, int ouslUnitId, boolean isStreamApplicable, boolean isNoneapplicable, int deliveryModeId, int academicYearID, boolean hasEntryExam, boolean hasCounselling, int counsellingMethodId, int counsellingRecListId) {
        this.programId = programId;
        this.code = code;
        this.title = title;
        this.facultyId = facultyId;
        this.department = department;
        StudyModeId = studyModeId;
        this.noOflevels = noOflevels;
        this.totalMinimumCredit = totalMinimumCredit;
        ProgramOfstudyId = programOfstudyId;
        this.duration = duration;
        this.introduceYear = introduceYear;
        this.startOfacademicYear = startOfacademicYear;
        this.hasDepartmentFacultyApproval = hasDepartmentFacultyApproval;
        this.hasGraduationRequest = hasGraduationRequest;
        this.isDisciplineApplicable = isDisciplineApplicable;
        this.installmentRate = installmentRate;
        this.elegibilityValidityPeriod = elegibilityValidityPeriod;
        DefultLevel = defultLevel;
        this.isExitPoint = isExitPoint;
        this.isFlatRateForNewRegistration = isFlatRateForNewRegistration;
        this.flatRateForNewRegistartion = flatRateForNewRegistartion;
        this.isFlatRateForReRegistration = isFlatRateForReRegistration;
        this.flatRateForReRegistartion = flatRateForReRegistartion;
        this.lateFee = lateFee;
        PararelProgramId = pararelProgramId;
        this.isSpecilizationApplicable = isSpecilizationApplicable;
        this.validityPeriod = validityPeriod;
        ExemptedMarks = exemptedMarks;
        ExemptedGradeId = exemptedGradeId;
        this.isdefineRateForReRegirstration = isdefineRateForReRegirstration;
        this.defineRateForReRegirstration = defineRateForReRegirstration;
        this.hasLocalStudent = hasLocalStudent;
        this.hasForiegnStudent = hasForiegnStudent;
        this.hasBothStudents = hasBothStudents;
        OfferedBy = offeredBy;
        OuslUnitId = ouslUnitId;
        this.isStreamApplicable = isStreamApplicable;
        this.isNoneapplicable = isNoneapplicable;
        DeliveryModeId = deliveryModeId;
        AcademicYearID = academicYearID;
        this.hasEntryExam = hasEntryExam;
        HasCounselling = hasCounselling;
        CounsellingMethodId = counsellingMethodId;
        CounsellingRecListId = counsellingRecListId;
    }

    public long getProgramId() {
        return programId;
    }

    public void setProgramId(long programId) {
        this.programId = programId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Faculty getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Faculty facultyId) {
        this.facultyId = facultyId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getStudyModeId() {
        return StudyModeId;
    }

    public void setStudyModeId(int studyModeId) {
        StudyModeId = studyModeId;
    }

    public String getNoOflevels() {
        return noOflevels;
    }

    public void setNoOflevels(String noOflevels) {
        this.noOflevels = noOflevels;
    }

    public float getTotalMinimumCredit() {
        return totalMinimumCredit;
    }

    public void setTotalMinimumCredit(float totalMinimumCredit) {
        this.totalMinimumCredit = totalMinimumCredit;
    }

    public int getProgramOfstudyId() {
        return ProgramOfstudyId;
    }

    public void setProgramOfstudyId(int programOfstudyId) {
        ProgramOfstudyId = programOfstudyId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getIntroduceYear() {
        return introduceYear;
    }

    public void setIntroduceYear(String introduceYear) {
        this.introduceYear = introduceYear;
    }

    public String getStartOfacademicYear() {
        return startOfacademicYear;
    }

    public void setStartOfacademicYear(String startOfacademicYear) {
        this.startOfacademicYear = startOfacademicYear;
    }

    public boolean isHasDepartmentFacultyApproval() {
        return hasDepartmentFacultyApproval;
    }

    public void setHasDepartmentFacultyApproval(boolean hasDepartmentFacultyApproval) {
        this.hasDepartmentFacultyApproval = hasDepartmentFacultyApproval;
    }

    public boolean isHasGraduationRequest() {
        return hasGraduationRequest;
    }

    public void setHasGraduationRequest(boolean hasGraduationRequest) {
        this.hasGraduationRequest = hasGraduationRequest;
    }

    public boolean isDisciplineApplicable() {
        return isDisciplineApplicable;
    }

    public void setDisciplineApplicable(boolean disciplineApplicable) {
        isDisciplineApplicable = disciplineApplicable;
    }

    public float getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(float installmentRate) {
        this.installmentRate = installmentRate;
    }

    public String getElegibilityValidityPeriod() {
        return elegibilityValidityPeriod;
    }

    public void setElegibilityValidityPeriod(String elegibilityValidityPeriod) {
        this.elegibilityValidityPeriod = elegibilityValidityPeriod;
    }

    public int getDefultLevel() {
        return DefultLevel;
    }

    public void setDefultLevel(int defultLevel) {
        DefultLevel = defultLevel;
    }

    public boolean isExitPoint() {
        return isExitPoint;
    }

    public void setExitPoint(boolean exitPoint) {
        isExitPoint = exitPoint;
    }

    public boolean isFlatRateForNewRegistration() {
        return isFlatRateForNewRegistration;
    }

    public void setFlatRateForNewRegistration(boolean flatRateForNewRegistration) {
        isFlatRateForNewRegistration = flatRateForNewRegistration;
    }

    public float getFlatRateForNewRegistartion() {
        return flatRateForNewRegistartion;
    }

    public void setFlatRateForNewRegistartion(float flatRateForNewRegistartion) {
        this.flatRateForNewRegistartion = flatRateForNewRegistartion;
    }

    public boolean isFlatRateForReRegistration() {
        return isFlatRateForReRegistration;
    }

    public void setFlatRateForReRegistration(boolean flatRateForReRegistration) {
        isFlatRateForReRegistration = flatRateForReRegistration;
    }

    public float getFlatRateForReRegistartion() {
        return flatRateForReRegistartion;
    }

    public void setFlatRateForReRegistartion(float flatRateForReRegistartion) {
        this.flatRateForReRegistartion = flatRateForReRegistartion;
    }

    public float getLateFee() {
        return lateFee;
    }

    public void setLateFee(float lateFee) {
        this.lateFee = lateFee;
    }

    public int getPararelProgramId() {
        return PararelProgramId;
    }

    public void setPararelProgramId(int pararelProgramId) {
        PararelProgramId = pararelProgramId;
    }

    public boolean isSpecilizationApplicable() {
        return isSpecilizationApplicable;
    }

    public void setSpecilizationApplicable(boolean specilizationApplicable) {
        isSpecilizationApplicable = specilizationApplicable;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public float getExemptedMarks() {
        return ExemptedMarks;
    }

    public void setExemptedMarks(float exemptedMarks) {
        ExemptedMarks = exemptedMarks;
    }

    public int getExemptedGradeId() {
        return ExemptedGradeId;
    }

    public void setExemptedGradeId(int exemptedGradeId) {
        ExemptedGradeId = exemptedGradeId;
    }

    public boolean isIsdefineRateForReRegirstration() {
        return isdefineRateForReRegirstration;
    }

    public void setIsdefineRateForReRegirstration(boolean isdefineRateForReRegirstration) {
        this.isdefineRateForReRegirstration = isdefineRateForReRegirstration;
    }

    public float getDefineRateForReRegirstration() {
        return defineRateForReRegirstration;
    }

    public void setDefineRateForReRegirstration(float defineRateForReRegirstration) {
        this.defineRateForReRegirstration = defineRateForReRegirstration;
    }

    public boolean isHasLocalStudent() {
        return hasLocalStudent;
    }

    public void setHasLocalStudent(boolean hasLocalStudent) {
        this.hasLocalStudent = hasLocalStudent;
    }

    public boolean isHasForiegnStudent() {
        return hasForiegnStudent;
    }

    public void setHasForiegnStudent(boolean hasForiegnStudent) {
        this.hasForiegnStudent = hasForiegnStudent;
    }

    public boolean isHasBothStudents() {
        return hasBothStudents;
    }

    public void setHasBothStudents(boolean hasBothStudents) {
        this.hasBothStudents = hasBothStudents;
    }

    public int getOfferedBy() {
        return OfferedBy;
    }

    public void setOfferedBy(int offeredBy) {
        OfferedBy = offeredBy;
    }

    public int getOuslUnitId() {
        return OuslUnitId;
    }

    public void setOuslUnitId(int ouslUnitId) {
        OuslUnitId = ouslUnitId;
    }

    public boolean isStreamApplicable() {
        return isStreamApplicable;
    }

    public void setStreamApplicable(boolean streamApplicable) {
        isStreamApplicable = streamApplicable;
    }

    public boolean isNoneapplicable() {
        return isNoneapplicable;
    }

    public void setNoneapplicable(boolean noneapplicable) {
        isNoneapplicable = noneapplicable;
    }

    public int getDeliveryModeId() {
        return DeliveryModeId;
    }

    public void setDeliveryModeId(int deliveryModeId) {
        DeliveryModeId = deliveryModeId;
    }

    public int getAcademicYearID() {
        return AcademicYearID;
    }

    public void setAcademicYearID(int academicYearID) {
        AcademicYearID = academicYearID;
    }

    public boolean isHasEntryExam() {
        return hasEntryExam;
    }

    public void setHasEntryExam(boolean hasEntryExam) {
        this.hasEntryExam = hasEntryExam;
    }

    public boolean isHasCounselling() {
        return HasCounselling;
    }

    public void setHasCounselling(boolean hasCounselling) {
        HasCounselling = hasCounselling;
    }

    public int getCounsellingMethodId() {
        return CounsellingMethodId;
    }

    public void setCounsellingMethodId(int counsellingMethodId) {
        CounsellingMethodId = counsellingMethodId;
    }

    public int getCounsellingRecListId() {
        return CounsellingRecListId;
    }

    public void setCounsellingRecListId(int counsellingRecListId) {
        CounsellingRecListId = counsellingRecListId;
    }


    @Override
    public String toString() {
        return "program{" +
                "programId=" + programId +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", facultyId=" + facultyId +
                ", department=" + department +
                ", StudyModeId=" + StudyModeId +
                ", noOflevels='" + noOflevels + '\'' +
                ", totalMinimumCredit=" + totalMinimumCredit +
                ", ProgramOfstudyId=" + ProgramOfstudyId +
                ", duration='" + duration + '\'' +
                ", introduceYear='" + introduceYear + '\'' +
                ", startOfacademicYear='" + startOfacademicYear + '\'' +
                ", hasDepartmentFacultyApproval=" + hasDepartmentFacultyApproval +
                ", hasGraduationRequest=" + hasGraduationRequest +
                ", isDisciplineApplicable=" + isDisciplineApplicable +
                ", installmentRate=" + installmentRate +
                ", elegibilityValidityPeriod='" + elegibilityValidityPeriod + '\'' +
                ", DefultLevel='" + DefultLevel + '\'' +
                ", isExitPoint=" + isExitPoint +
                ", isFlatRateForNewRegistration=" + isFlatRateForNewRegistration +
                ", flatRateForNewRegistartion=" + flatRateForNewRegistartion +
                ", isFlatRateForReRegistration=" + isFlatRateForReRegistration +
                ", flatRateForReRegistartion=" + flatRateForReRegistartion +
                ", lateFee=" + lateFee +
                ", PararelProgramId=" + PararelProgramId +
                ", isSpecilizationApplicable=" + isSpecilizationApplicable +
                ", validityPeriod='" + validityPeriod + '\'' +
                ", ExemptedMarks=" + ExemptedMarks +
                ", ExemptedGradeId=" + ExemptedGradeId +
                ", isdefineRateForReRegirstration=" + isdefineRateForReRegirstration +
                ", defineRateForReRegirstration=" + defineRateForReRegirstration +
                ", hasLocalStudent=" + hasLocalStudent +
                ", hasForiegnStudent=" + hasForiegnStudent +
                ", hasBothStudents=" + hasBothStudents +
                ", OfferedBy=" + OfferedBy +
                ", OuslUnitId=" + OuslUnitId +
                ", isStreamApplicable=" + isStreamApplicable +
                ", isNoneapplicable=" + isNoneapplicable +
                ", DeliveryModeId=" + DeliveryModeId +
                ", AcademicYearID=" + AcademicYearID +
                ", hasEntryExam=" + hasEntryExam +
                ", HasCounselling=" + HasCounselling +
                ", CounsellingMethodId=" + CounsellingMethodId +
                ", CounsellingRecListId=" + CounsellingRecListId +
                '}';
    }
}
