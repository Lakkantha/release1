package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table
public class PostalCode implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String description;

    public PostalCode() {
    }

    public PostalCode(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PostalCode{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
