package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class AssociateProgramAssociateCourse implements SuperEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "asc_program_id", referencedColumnName = "asp_associateProgramId", nullable = false)
    private AssociateProgram associateProgram;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "asc_course_id", referencedColumnName = "asc_associatecourseId", nullable = false)
    private AssociateCourse associateCourse;
    private int level;
    private Boolean isCompulsory;
    private Boolean isPrerequisites;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;

    private int userLastUpdated;

    public AssociateProgramAssociateCourse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AssociateProgram getAssociateProgram() {
        return associateProgram;
    }

    public void setAssociateProgram(AssociateProgram associateProgram) {
        this.associateProgram = associateProgram;
    }

    public AssociateCourse getAssociateCourse() {
        return associateCourse;
    }

    public void setAssociateCourse(AssociateCourse associateCourse) {
        this.associateCourse = associateCourse;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Boolean getCompulsory() {
        return isCompulsory;
    }

    public void setCompulsory(Boolean compulsory) {
        isCompulsory = compulsory;
    }

    public Boolean getPrerequisites() {
        return isPrerequisites;
    }

    public void setPrerequisites(Boolean prerequisites) {
        isPrerequisites = prerequisites;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    @Override
    public String toString() {
        return "AssociateProgramAssociateCourse{" +
                "id=" + id +
                ", associateProgram=" + associateProgram +
                ", associateCourse=" + associateCourse +
                ", level=" + level +
                ", isCompulsory=" + isCompulsory +
                ", isPrerequisites=" + isPrerequisites +
                ", userCreatedDate=" + userCreatedDate +
                ", userLastUpdated=" + userLastUpdated +
                '}';
    }
}
