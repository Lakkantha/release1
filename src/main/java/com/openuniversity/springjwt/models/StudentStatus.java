package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
public class StudentStatus implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String description;



    public StudentStatus() {
    }

    public StudentStatus(long id, String description) {
        this.id = id;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
