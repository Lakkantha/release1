package com.openuniversity.springjwt.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "confirmationtoken")
public class ConfirmationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="token_id")
    private long tokenid;

    @NotBlank
    @Column(name="confirmation_token")
    private String confirmationToken;

    @NotBlank
    @Column(name="validornot")
    private String validornot;

    @NotBlank
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @NotBlank
    @OneToOne(targetEntity = User_Applicant.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User_Applicant userApplicant;

    public ConfirmationToken() {
    }

    public ConfirmationToken(User_Applicant userApplicant) {
        this.userApplicant = userApplicant;
        createdDate = new Date();
        confirmationToken = UUID.randomUUID().toString();
        setConfirmationToken(confirmationToken);
        setCreatedDate(createdDate);
        setUser(userApplicant);
        setValidOrNot(validornot);
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User_Applicant getUser() {
        return userApplicant;
    }

    public void setUser(User_Applicant userApplicant) {
        this.userApplicant = userApplicant;
    }

    public long getTokenid() {
        return tokenid;
    }

    public void setTokenid(long tokenid) {
        this.tokenid = tokenid;
    }

    public void setValidOrNot(String validOrNot){this.validornot = validOrNot;}

    public String getValidOrNot(String token){return validornot;}
}
