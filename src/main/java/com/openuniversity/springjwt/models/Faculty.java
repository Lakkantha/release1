package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name="Faculty")

public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    private int id;
    private String code;
    private String description;
    private String emp_employeeCode_dean;
    private String numberOfDepartments;
    private String emp_employeeCode_assistant;
    private String IntergrationCode;


    public Faculty(){

    }

    public Faculty(int id, String code, String description, String emp_employeeCode_dean, String numberOfDepartments, String emp_employeeCode_assistant, String intergrationCode) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.emp_employeeCode_dean = emp_employeeCode_dean;
        this.numberOfDepartments = numberOfDepartments;
        this.emp_employeeCode_assistant = emp_employeeCode_assistant;
        IntergrationCode = intergrationCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmp_employeeCode_dean() {
        return emp_employeeCode_dean;
    }

    public void setEmp_employeeCode_dean(String emp_employeeCode_dean) {
        this.emp_employeeCode_dean = emp_employeeCode_dean;
    }

    public String getNumberOfDepartments() {
        return numberOfDepartments;
    }

    public void setNumberOfDepartments(String numberOfDepartments) {
        this.numberOfDepartments = numberOfDepartments;
    }

    public String getEmp_employeeCode_assistant() {
        return emp_employeeCode_assistant;
    }

    public void setEmp_employeeCode_assistant(String emp_employeeCode_assistant) {
        this.emp_employeeCode_assistant = emp_employeeCode_assistant;
    }

    public String getIntergrationCode() {
        return IntergrationCode;
    }

    public void setIntergrationCode(String intergrationCode) {
        IntergrationCode = intergrationCode;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", emp_employeeCode_dean='" + emp_employeeCode_dean + '\'' +
                ", numberOfDepartments='" + numberOfDepartments + '\'' +
                ", emp_employeeCode_assistant='" + emp_employeeCode_assistant + '\'' +
                ", IntergrationCode='" + IntergrationCode + '\'' +
                '}';
    }
}
