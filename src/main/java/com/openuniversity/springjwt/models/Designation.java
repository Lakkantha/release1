package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "designation")
public class Designation {

    private long id;
    private String designation;

    public Designation() {

    }

    public Designation(String designation) {
        this.designation = designation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "designation", nullable = false)
    public String getDesignation() {
        return designation;
    }
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public String toString() {
        return "Designation [id=" + id + ", designation=" + designation + "]";
    }

}

