package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "olcertificates")
public class OLCertificates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "certifiate_name")
    private String certificateName;

    public OLCertificates() {
    }

    public OLCertificates(String certificateName) {
        this.certificateName = certificateName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }
}
