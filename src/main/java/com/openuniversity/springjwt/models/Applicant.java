package com.openuniversity.springjwt.models;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Applicant implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private int title;
    @Column(nullable = false)
    private int gender;


    @Column(nullable = false)
    private java.sql.Date dateOfBirth;

    @Column(nullable = false)
    private String nationality;

    private String email;
    @Column(name="permenetAddress",nullable = false)
    private String foriegnAddress;
    @Column(name ="corespondanceAddress")
    private String localAddress;
    @Column(name="mobileNo",nullable = false)
    private String mobileNoForiegn;
    @Column(name="contact_nofix")
    private String mobileNoLocal;

    private int archived;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;
    private int userLastUpdated;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "initialstudent_id",referencedColumnName = "id",nullable = false)
    private InitialStudent initialStudent;

    public Applicant() {
    }

    public Applicant(int id, int title, int gender, java.sql.Date dateOfBirth, String nationality, String email, String foriegnAddress, String localAddress, String mobileNoForiegn, String mobileNoLocal, int archived, Date userCreatedDate, int userLastUpdated, InitialStudent initialStudent) {
        this.id = id;
        this.title = title;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
        this.email = email;
        this.foriegnAddress = foriegnAddress;
        this.localAddress = localAddress;
        this.mobileNoForiegn = mobileNoForiegn;
        this.mobileNoLocal = mobileNoLocal;
        this.archived = archived;
        this.userCreatedDate = userCreatedDate;
        this.userLastUpdated = userLastUpdated;
        this.initialStudent = initialStudent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public java.sql.Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.sql.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForiegnAddress() {
        return foriegnAddress;
    }

    public void setForiegnAddress(String foriegnAddress) {
        this.foriegnAddress = foriegnAddress;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String localAddress) {
        this.localAddress = localAddress;
    }

    public String getMobileNoForiegn() {
        return mobileNoForiegn;
    }

    public void setMobileNoForiegn(String mobileNoForiegn) {
        this.mobileNoForiegn = mobileNoForiegn;
    }

    public String getMobileNoLocal() {
        return mobileNoLocal;
    }

    public void setMobileNoLocal(String mobileNoLocal) {
        this.mobileNoLocal = mobileNoLocal;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public InitialStudent getInitialStudent() {
        return initialStudent;
    }

    public void setInitialStudent(InitialStudent initialStudent) {
        this.initialStudent = initialStudent;
    }

    @Override
    public String toString() {
        return "Applicant{" +
                "id=" + id +
                ", title=" + title +
                ", gender=" + gender +
                ", dateOfBirth=" + dateOfBirth +
                ", nationality='" + nationality + '\'' +
                ", email='" + email + '\'' +
                ", foriegnAddress='" + foriegnAddress + '\'' +
                ", localAddress='" + localAddress + '\'' +
                ", mobileNoForiegn='" + mobileNoForiegn + '\'' +
                ", mobileNoLocal='" + mobileNoLocal + '\'' +
                ", archived=" + archived +
                ", userCreatedDate=" + userCreatedDate +
                ", userLastUpdated=" + userLastUpdated +
                ", initialStudent=" + initialStudent +
                '}';
    }
}
