package com.openuniversity.springjwt.models;
import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String> {


    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
    /*
    if (authentication == null || !authentication.isAuthenticated()) {
        return null;
    }
   */
        if (name != "anonymousUser" ) {
            return Optional.of(name);
        }
        else {
            return Optional.of ("System");
        }
    }
}	
	