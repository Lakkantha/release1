package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class ProgramStarted  implements SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long programStartedId;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "program_id", referencedColumnName = "programId", nullable = false)
    private
    Program program;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "student_type_id", referencedColumnName = "stp_studentTypeId", nullable = false)
    private
    StudentType studentType;

    private Boolean multipleBatchApplicable;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "academicYearId", referencedColumnName = "acy_academicYearId", nullable = false)
    private
    AcademicYear academicYear;

    private String batchNo;
    private String programScheduleCode;
    private Date fromDate;
    private Date toDate;
    private Double localFee;
    private Double foreignFee;


    public ProgramStarted() {
    }

    public ProgramStarted(long programStartedId, Program program, StudentType studentType, Boolean multipleBatchApplicable, AcademicYear academicYear, String batchNo, String programScheduleCode, Date fromDate, Date toDate, Double localFee, Double foreignFee) {
        this.programStartedId = programStartedId;
        this.program = program;
        this.studentType = studentType;
        this.multipleBatchApplicable = multipleBatchApplicable;
        this.academicYear = academicYear;
        this.batchNo = batchNo;
        this.programScheduleCode = programScheduleCode;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.localFee = localFee;
        this.foreignFee = foreignFee;
    }

    public long getProgramStartedId() {
        return programStartedId;
    }

    public void setProgramStartedId(long programStartedId) {
        this.programStartedId = programStartedId;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public StudentType getStudentType() {
        return studentType;
    }

    public void setStudentType(StudentType studentType) {
        this.studentType = studentType;
    }

    public Boolean getMultipleBatchApplicable() {
        return multipleBatchApplicable;
    }

    public void setMultipleBatchApplicable(Boolean multipleBatchApplicable) {
        this.multipleBatchApplicable = multipleBatchApplicable;
    }

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getProgramScheduleCode() {
        return programScheduleCode;
    }

    public void setProgramScheduleCode(String programScheduleCode) {
        this.programScheduleCode = programScheduleCode;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Double getLocalFee() {
        return localFee;
    }

    public void setLocalFee(Double localFee) {
        this.localFee = localFee;
    }

    public Double getForeignFee() {
        return foreignFee;
    }

    public void setForeignFee(Double foreignFee) {
        this.foreignFee = foreignFee;
    }
}
