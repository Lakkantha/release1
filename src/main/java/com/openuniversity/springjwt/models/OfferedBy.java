package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name=" rg_p_offeredBy")
public class OfferedBy implements SuperEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ofb_offeredById")
    private int id;
    @Column(name = "ofb_description")
    private String description;

    public OfferedBy() {
    }

    public OfferedBy(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
