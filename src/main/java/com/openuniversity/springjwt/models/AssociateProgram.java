package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
public class AssociateProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long asp_associateProgramId;

    @Column(unique = true)
    private String aspCode;

    private String aspTitle;

    @ManyToOne
    @JoinColumn(name = "offeredById", referencedColumnName="ofb_offeredById")
    private OfferedBy  offeredBy;

    @ManyToOne
    @JoinColumn(name = "fac_facultyId", referencedColumnName="id", nullable = false)
    private Faculty facultyId;

    @ManyToOne
    @JoinColumn(name = "dep_departmentId", referencedColumnName="departmentId", nullable = false)
    private Department department;

    @ManyToOne
    @JoinColumn(name = "oun_ouslunitId", referencedColumnName="oun_ouslunitId", nullable = false)
    private OuslUnit ouslUnit;

    @ManyToOne
    @JoinColumn(name = "dlm_deliveryModeId", referencedColumnName="dlm_deliveryModeId", nullable = false)
    private DeliveryMode deliveryMode;

    @ManyToOne
    @JoinColumn(name = "prg_programId", referencedColumnName="programId", nullable = false)
    private Program program;

    public AssociateProgram() {
    }

    public AssociateProgram(long asp_associateProgramId, String aspCode, String aspTitle, OfferedBy offeredBy, Faculty facultyId, Department department, OuslUnit ouslUnit, DeliveryMode deliveryMode, Program program, boolean asp_isFlatRateForNewregistration, double asp_LateFee) {
        this.asp_associateProgramId = asp_associateProgramId;
        this.aspCode = aspCode;
        this.aspTitle = aspTitle;
        this.offeredBy = offeredBy;
        this.facultyId = facultyId;
        this.department = department;
        this.ouslUnit = ouslUnit;
        this.deliveryMode = deliveryMode;
        this.program = program;
        this.asp_isFlatRateForNewregistration = asp_isFlatRateForNewregistration;
        this.asp_LateFee = asp_LateFee;
    }

    private boolean asp_isFlatRateForNewregistration;
    private double asp_LateFee;


    public long getAsp_associateProgramId() {
        return asp_associateProgramId;
    }

    public void setAsp_associateProgramId(long asp_associateProgramId) {
        this.asp_associateProgramId = asp_associateProgramId;
    }

    public String getAspCode() {
        return aspCode;
    }

    public void setAspCode(String aspCode) {
        this.aspCode = aspCode;
    }

    public String getAspTitle() {
        return aspTitle;
    }

    public void setAspTitle(String aspTitle) {
        this.aspTitle = aspTitle;
    }

    public OfferedBy getOfferedBy() {
        return offeredBy;
    }

    public void setOfferedBy(OfferedBy offeredBy) {
        this.offeredBy = offeredBy;
    }

    public Faculty getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Faculty facultyId) {
        this.facultyId = facultyId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public OuslUnit getOuslUnit() {
        return ouslUnit;
    }

    public void setOuslUnit(OuslUnit ouslUnit) {
        this.ouslUnit = ouslUnit;
    }

    public DeliveryMode getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(DeliveryMode deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public boolean isAsp_isFlatRateForNewregistration() {
        return asp_isFlatRateForNewregistration;
    }

    public void setAsp_isFlatRateForNewregistration(boolean asp_isFlatRateForNewregistration) {
        this.asp_isFlatRateForNewregistration = asp_isFlatRateForNewregistration;
    }

    public double getAsp_LateFee() {
        return asp_LateFee;
    }

    public void setAsp_LateFee(double asp_LateFee) {
        this.asp_LateFee = asp_LateFee;
    }

    @Override
    public String toString() {
        return "AssociateProgram{" +
                "asp_associateProgramId=" + asp_associateProgramId +
                ", aspCode='" + aspCode + '\'' +
                ", aspTitle='" + aspTitle + '\'' +
                ", offeredBy=" + offeredBy +
                ", facultyId=" + facultyId +
                ", department=" + department +
                ", ouslUnit=" + ouslUnit +
                ", deliveryMode=" + deliveryMode +
                ", program=" + program +
                ", asp_isFlatRateForNewregistration=" + asp_isFlatRateForNewregistration +
                ", asp_LateFee=" + asp_LateFee +
                '}';
    }
}
