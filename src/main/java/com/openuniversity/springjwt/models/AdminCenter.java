package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name="AdminCenter")
public class AdminCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int adc_adminCenterId;

  private int pac_programAcademicyearId;
  private int cnt_centerId_FK;

  public AdminCenter(){

  }

    public AdminCenter(int adc_adminCenterId, int pac_programAcademicyearId, int cnt_centerId_FK) {
        this.adc_adminCenterId = adc_adminCenterId;
        this.pac_programAcademicyearId = pac_programAcademicyearId;
        this.cnt_centerId_FK = cnt_centerId_FK;
    }

    public int getAdc_adminCenterId() {
        return adc_adminCenterId;
    }

    public void setAdc_adminCenterId(int adc_adminCenterId) {
        this.adc_adminCenterId = adc_adminCenterId;
    }

    public int getPac_programAcademicyearId() {
        return pac_programAcademicyearId;
    }

    public void setPac_programAcademicyearId(int pac_programAcademicyearId) {
        this.pac_programAcademicyearId = pac_programAcademicyearId;
    }

    public int getCnt_centerId_FK() {
        return cnt_centerId_FK;
    }

    public void setCnt_centerId_FK(int cnt_centerId_FK) {
        this.cnt_centerId_FK = cnt_centerId_FK;
    }

    @Override
    public String toString() {
        return "AdminCenter{" +
                "adc_adminCenterId=" + adc_adminCenterId +
                ", pac_programAcademicyearId=" + pac_programAcademicyearId +
                ", cnt_centerId_FK=" + cnt_centerId_FK +
                '}';
    }
}
