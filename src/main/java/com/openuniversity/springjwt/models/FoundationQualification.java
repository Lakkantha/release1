package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table (name = "ouslfoundation")
public class FoundationQualification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "reg_number")
    private int regNumber;

    @Column(name = "year")
    private int year;

    @Column(name = "stream")
    private String stream;

    @Column(name = "medium")
    private String medium;

    @Column(name = "subject")
    private String subject;

    @Column(name = "result")
    private String result;

    public FoundationQualification() {
    }

    public FoundationQualification(Long id, int regNumber, int year, String stream, String medium, String subject, String result) {
        this.id = id;
        this.regNumber = regNumber;
        this.year = year;
        this.stream = stream;
        this.medium = medium;
        this.subject = subject;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(int regNumber) {
        this.regNumber = regNumber;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
