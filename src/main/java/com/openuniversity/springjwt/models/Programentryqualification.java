package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name = "Programentryqualification ")
public class Programentryqualification {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;

    private long entryqualification_id;
    private long prog_id;

    public Programentryqualification(){

    }

    public Programentryqualification(long id, long entryqualification_id, long prog_id) {
        this.id = id;
        this.entryqualification_id = entryqualification_id;
        this.prog_id = prog_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEntryqualification_id() {
        return entryqualification_id;
    }

    public void setEntryqualification_id(long entryqualification_id) {
        this.entryqualification_id = entryqualification_id;
    }

    public long getProg_id() {
        return prog_id;
    }

    public void setProg_id(long prog_id) {
        this.prog_id = prog_id;
    }

    @Override
    public String toString() {
        return "Programentryqualification{" +
                "id=" + id +
                ", entryqualification_id=" + entryqualification_id +
                ", prog_id=" + prog_id +
                '}';
    }
}
