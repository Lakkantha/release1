package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name=" rg_m_ouslunit")
public class OuslUnit implements SuperEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oun_ouslunitId")
    private int id;
    @Column(name = "oun_code")
    private String code;
    @Column(name = "oun_description")
    private String description;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "fac_facultyId", referencedColumnName = "id", nullable = false)
    private Faculty faculty;

    public OuslUnit() {
    }

    public OuslUnit(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
