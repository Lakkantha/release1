package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name = "rg_p_studentType")
public class StudentType  implements SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stp_studentTypeId")
    private long id;

    @Column(name = "stp_description")
    private String description;

    public StudentType() {
    }

    public StudentType(long id, String description) {
        this.id = id;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
