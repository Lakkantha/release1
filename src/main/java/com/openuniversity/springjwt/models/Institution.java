package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "institution")
public class Institution {

    private long id;
    private String institution;

    public Institution() {

    }

    public Institution(String institution) {
        this.institution = institution;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
            this.id = id;
        }

    @Column(name = "institution", nullable = false)
    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    @Override
    public String toString() {
        return "Institution [id=" + id + ", institution=" + institution + "]";
    }

}
