package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "workexperience")
public class Experience {

    private long id;
    private String company;
    private String designation;
    private Date dateFrom;
    private Date dateTo;
    private String certificates;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "applicant_id", referencedColumnName = "id", nullable = false)
    private Certificates applicant;

    public Experience() {
    }

    public Experience(String company, String designation, Date dateFrom, Date dateTo, String certificates) {
        this.company = company;
        this.designation = designation;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.certificates = certificates;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "certificates", nullable = true)
    public String getCertificates() { return certificates; }
    public void setCertificates(String certificates) { this.certificates = certificates; }

    @Column(name = "company", nullable = false)
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @Column(name = "designation", nullable = false)
    public String getDesignation() {
        return designation;
    }
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Column(name = "dateFrom", nullable = false)
    public Date getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Column(name = "dateTo", nullable = false)
    public Date getDateTo() {
        return dateTo;
    }
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return "Experience [id=" + id + ", company=" + company + ", designation=" + designation + ", dateFrom=" + dateFrom
                + ", dateTo=" + dateTo + ", certificates=" + certificates + "]";
    }

}
