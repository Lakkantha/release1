package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name = "Medium")
public class Medium {


        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
         private long mediumId;
         private String description;

         public Medium(){

         }

    public Medium(long mediumId, String description) {
        this.mediumId = mediumId;
        this.description = description;
    }

    public long getMediumId() {
        return mediumId;
    }

    public void setMediumId(long mediumId) {
        this.mediumId = mediumId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Medium{" +
                "mediumId=" + mediumId +
                ", description='" + description + '\'' +
                '}';
    }
}
