package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "company")
public class Company {

    private long id;
    private String company;

    public Company() {

    }

    public Company(String company) {
        this.company = company;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "company", nullable = false)
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Company [id=" + id + ", company=" + company + "]";
    }

}

