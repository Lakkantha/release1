package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "certificates")
public class Certificates {

    private long id;
    private String certificates;

    public Certificates() {

    }

    public Certificates(String certificates) {
        this.certificates = certificates;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "certificates", nullable = false)
    public String getCertificates() {
        return certificates;
    }
    public void setCertificates(String certificate) {
        this.certificates = certificate;
    }

    @Override
    public String toString() {
        return "Certificates [id=" + id + ", certificates=" + certificates + "]";
    }

}
