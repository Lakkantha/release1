package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "olsubjects")
public class OLSubjects {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "ol_subject_name")
    private String subject;

    public OLSubjects() {
    }

    public OLSubjects(Long id, String subject) {
        this.id = id;
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
