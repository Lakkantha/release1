package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name="Department")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int departmentId;

    private String code;
    private String description;
    private String emp_employeeCode_department;
    private String intergrationCode;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "facultyId", referencedColumnName="id", nullable = false)
    private Faculty facultyId;


    public Department(){

    }

    public Department(int departmentId, String code, String description, String emp_employeeCode_department, String intergrationCode, Faculty facultyId) {
        this.departmentId = departmentId;
        this.code = code;
        this.description = description;
        this.emp_employeeCode_department = emp_employeeCode_department;
        this.intergrationCode = intergrationCode;
        this.facultyId = facultyId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmp_employeeCode_department() {
        return emp_employeeCode_department;
    }

    public void setEmp_employeeCode_department(String emp_employeeCode_department) {
        this.emp_employeeCode_department = emp_employeeCode_department;
    }

    public String getIntergrationCode() {
        return intergrationCode;
    }

    public void setIntergrationCode(String intergrationCode) {
        this.intergrationCode = intergrationCode;
    }

    public Faculty getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Faculty facultyId) {
        this.facultyId = facultyId;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + departmentId +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", emp_employeeCode_department='" + emp_employeeCode_department + '\'' +
                ", intergrationCode='" + intergrationCode + '\'' +
                ", facultyId=" + facultyId +
                '}';
    }
}
