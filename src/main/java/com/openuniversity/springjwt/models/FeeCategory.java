package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "feecategory")
public class FeeCategory {

    private long id;
    private String description;
    private boolean isrefundable;

    public FeeCategory() {
    }

    public FeeCategory(String description, boolean isrefundable) {
        description = description;
        this.isrefundable = isrefundable;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    @Column(name = "description", nullable = false)
    public void setDescription(String description) {
        description = description;
    }

    @Column(name = "isrefundable", nullable = false)
    public boolean isIsrefundable() {
        return isrefundable;
    }
    public void setIsrefundable(boolean isrefundable) {
        this.isrefundable = isrefundable;
    }

    @Override
    public String toString() {
        return "FeeCategory{" +
                "id=" + id +
                ", Description='" + description + '\'' +
                ", isrefundable=" + isrefundable +
                '}';
    }
}
