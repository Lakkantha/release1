package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name = "AcdemicCenter")
public class AcademicCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long pac_programAcademicyearId;
    private long cnt_centerId;


    public AcademicCenter(){

    }

    public AcademicCenter(long id, long pac_programAcademicyearId, long cnt_centerId) {
        this.id = id;
        this.pac_programAcademicyearId = pac_programAcademicyearId;
        this.cnt_centerId = cnt_centerId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPac_programAcademicyearId() {
        return pac_programAcademicyearId;
    }

    public void setPac_programAcademicyearId(long pac_programAcademicyearId) {
        this.pac_programAcademicyearId = pac_programAcademicyearId;
    }

    public long getCnt_centerId() {
        return cnt_centerId;
    }

    public void setCnt_centerId(long cnt_centerId) {
        this.cnt_centerId = cnt_centerId;
    }
}
