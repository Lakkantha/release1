package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
public class AssociateCourse implements SuperEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "asc_associatecourseId")
    private long id;

    @Column(unique = true,nullable = false)
    private String ascCode;

    private String  ascDescription;

    private int credit;

    private Double additionalFee;

    public AssociateCourse() {
    }

    public AssociateCourse(long id, String ascCode, String ascDescription, int credit, Double additionalFee) {
        this.id = id;
        this.ascCode = ascCode;
        this.ascDescription = ascDescription;
        this.credit = credit;
        this.additionalFee = additionalFee;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAscCode() {
        return ascCode;
    }

    public void setAscCode(String ascCode) {
        this.ascCode = ascCode;
    }

    public String getAscDescription() {
        return ascDescription;
    }

    public void setAscDescription(String ascDescription) {
        this.ascDescription = ascDescription;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public Double getAdditionalFee() {
        return additionalFee;
    }

    public void setAdditionalFee(Double additionalFee) {
        this.additionalFee = additionalFee;
    }

    @Override
    public String toString() {
        return "AssociateCourse{" +
                "id=" + id +
                ", ascCode='" + ascCode + '\'' +
                ", ascDescription='" + ascDescription + '\'' +
                ", credit=" + credit +
                ", additionalFee=" + additionalFee +
                '}';
    }
}
