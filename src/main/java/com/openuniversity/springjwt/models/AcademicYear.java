package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "rg_m_academicyear")
public class AcademicYear implements SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "acy_academicYearId")
    private int id;

    @Column(name = "acy_year")
    private
    String year;

    public AcademicYear() {
    }

    public AcademicYear(int id, String year) {
        this.id = id;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
