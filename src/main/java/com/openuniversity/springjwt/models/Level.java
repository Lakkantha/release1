package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Level implements SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "user_created_date")
    private Date userCreatedDate;

    private int userLastUpdated;

    public Level() {
    }

    public Level(long id, String description, Date userCreatedDate, int userLastUpdated) {
        this.id = id;
        this.description = description;
        this.userCreatedDate = userCreatedDate;
        this.userLastUpdated = userLastUpdated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public int getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(int userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
}
