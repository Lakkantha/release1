package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name="InitialStudent")
public class InitialStudent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "programstartedid", referencedColumnName="programStartedId", nullable = false)
    private ProgramStarted programStarted;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "streamid", referencedColumnName="id", nullable = false)
    private Stream stream;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "specializationid", referencedColumnName="id", nullable = false)
    private Specialization specialization;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "academiccenterid", referencedColumnName="id", nullable = false)
    private AcademicCenter academicCenter;


    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "admincenterid", referencedColumnName="adc_adminCenterId", nullable = false)
    private AdminCenter adminCenter;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "med_mediumId", referencedColumnName="mediumId", nullable = false)
    private Medium medium;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "programentryqualificationid", referencedColumnName="id", nullable = false)
    private Programentryqualification programentryqualification;

    private String nic;
    private String correspondanceaddress;

    private String namewithinitials;
    private String mobileno;
    private String faxno;

    private String email;
    private String mobileverifieid;
    private String emailverifiedid;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "applicantype_id", referencedColumnName="id", nullable = false)
    private ApplicantType applicantType;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "applicant_id", referencedColumnName="id", nullable = false)
    private InitialApplicant initialApplicant;


    public InitialStudent() {
    }

    public InitialStudent(long id, ProgramStarted programStarted, Stream stream, Specialization specialization, AcademicCenter academicCenter, AdminCenter adminCenter, Medium medium, Programentryqualification programentryqualification, String nic, String correspondanceaddress, String namewithinitials, String mobileno, String faxno, String email, String mobileverifieid, String emailverifiedid, ApplicantType applicantType, InitialApplicant initialApplicant) {
        this.id = id;
        this.programStarted = programStarted;
        this.stream = stream;
        this.specialization = specialization;
        this.academicCenter = academicCenter;
        this.adminCenter = adminCenter;
        this.medium = medium;
        this.programentryqualification = programentryqualification;
        this.nic = nic;
        this.correspondanceaddress = correspondanceaddress;
        this.namewithinitials = namewithinitials;
        this.mobileno = mobileno;
        this.faxno = faxno;
        this.email = email;
        this.mobileverifieid = mobileverifieid;
        this.emailverifiedid = emailverifiedid;
        this.applicantType = applicantType;
        this.initialApplicant = initialApplicant;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProgramStarted getProgramStarted() {
        return programStarted;
    }

    public void setProgramStarted(ProgramStarted programStarted) {
        this.programStarted = programStarted;
    }

    public Stream getStream() {
        return stream;
    }

    public void setStream(Stream stream) {
        this.stream = stream;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public AcademicCenter getAcademicCenter() {
        return academicCenter;
    }

    public void setAcademicCenter(AcademicCenter academicCenter) {
        this.academicCenter = academicCenter;
    }

    public AdminCenter getAdminCenter() {
        return adminCenter;
    }

    public void setAdminCenter(AdminCenter adminCenter) {
        this.adminCenter = adminCenter;
    }

    public Medium getMedium() {
        return medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    public Programentryqualification getProgramentryqualification() {
        return programentryqualification;
    }

    public void setProgramentryqualification(Programentryqualification programentryqualification) {
        this.programentryqualification = programentryqualification;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getCorrespondanceaddress() {
        return correspondanceaddress;
    }

    public void setCorrespondanceaddress(String correspondanceaddress) {
        this.correspondanceaddress = correspondanceaddress;
    }

    public String getNamewithinitials() {
        return namewithinitials;
    }

    public void setNamewithinitials(String namewithinitials) {
        this.namewithinitials = namewithinitials;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getFaxno() {
        return faxno;
    }

    public void setFaxno(String faxno) {
        this.faxno = faxno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileverifieid() {
        return mobileverifieid;
    }

    public void setMobileverifieid(String mobileverifieid) {
        this.mobileverifieid = mobileverifieid;
    }

    public String getEmailverifiedid() {
        return emailverifiedid;
    }

    public void setEmailverifiedid(String emailverifiedid) {
        this.emailverifiedid = emailverifiedid;
    }

    public ApplicantType getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(ApplicantType applicantType) {
        this.applicantType = applicantType;
    }

    public InitialApplicant getInitialApplicant() {
        return initialApplicant;
    }

    public void setInitialApplicant(InitialApplicant initialApplicant) {
        this.initialApplicant = initialApplicant;
    }

    @Override
    public String toString() {
        return "InitialStudent{" +
                "id=" + id +
                ", programStarted=" + programStarted +
                ", stream=" + stream +
                ", specialization=" + specialization +
                ", academicCenter=" + academicCenter +
                ", adminCenter=" + adminCenter +
                ", medium=" + medium +
                ", programentryqualification=" + programentryqualification +
                ", nic='" + nic + '\'' +
                ", correspondanceaddress='" + correspondanceaddress + '\'' +
                ", namewithinitials='" + namewithinitials + '\'' +
                ", mobileno='" + mobileno + '\'' +
                ", faxno='" + faxno + '\'' +
                ", email='" + email + '\'' +
                ", mobileverifieid='" + mobileverifieid + '\'' +
                ", emailverifiedid='" + emailverifiedid + '\'' +
                ", applicantType=" + applicantType +
                ", initialApplicant=" + initialApplicant +
                '}';
    }
}
