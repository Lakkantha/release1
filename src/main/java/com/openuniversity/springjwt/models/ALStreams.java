package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "alstreams")
public class ALStreams {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "stream")
    private String stream;

    public ALStreams() {
    }

    public ALStreams(Long id, String stream) {
        this.id = id;
        this.stream = stream;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }
}
