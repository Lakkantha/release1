package com.openuniversity.springjwt.models;


import javax.persistence.*;

@Entity
@Table(name = "ApplicantType")
public class ApplicantType {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private long id;

   private String description;

   public ApplicantType(){

   }

    public ApplicantType(long id, String description) {
        this.id = id;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ApplicantType{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
