package com.openuniversity.springjwt.models;

import javax.persistence.*;

@Entity
@Table(name = "applicablefee")
public class Applicablefee {

    private long id;
    private  String category;
    private String description;

    public Applicablefee() {
    }

    public Applicablefee(long id, String category, String description) {
        this.id = id;
        this.category = category;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "category", nullable = false)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "applicablefee{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
