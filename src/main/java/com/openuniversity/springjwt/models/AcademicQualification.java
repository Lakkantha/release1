package com.openuniversity.springjwt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "academicqualification")
public class AcademicQualification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "qualification_type")
    private String qualificationType;

    @Column(name = "qualification_name")
    private String qualificationName;

    @Column(name = "duration")
    private String duration;

    @Column(name = "institution")
    private String institution;

    @Column(name = "effectiveDate")
    private Date effectiveDate;

    @Column(name = "certifiate_name")
    private String certificateName;

    public AcademicQualification() {
    }

    public AcademicQualification(Long id, String qualificationType, String qualificationName, String duration, String institution, Date effectiveDate, String certificateName) {
        this.id = id;
        this.qualificationType = qualificationType;
        this.qualificationName = qualificationName;
        this.duration = duration;
        this.institution = institution;
        this.effectiveDate = effectiveDate;
        this.certificateName = certificateName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(String qualificationType) {
        this.qualificationType = qualificationType;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }
}
